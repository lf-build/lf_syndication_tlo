﻿using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Security.Encryption;
using LendFoundry.Tenant.Client;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Serializers;
using MongoDB.Driver;
using System;
using System.Threading.Tasks;
using MongoDB.Driver.Linq;


namespace LendFoundry.Syndication.Tloxp.Persistence
{
    public class ComprehensivePersonReportRepository : MongoRepository<IComprehensivePersonReport, ComprehensivePersonReport>, IComprehensivePersonReportRepository
    {
        public ComprehensivePersonReportRepository(IMongoConfiguration mongoConfiguration, ITenantService tenantService, ITenantTime tenantTime, IEncryptionService encryptionService)
             : base(tenantService, mongoConfiguration, "comprehensive-person")
        {
            if (!BsonClassMap.IsClassMapRegistered(typeof(ComprehensivePersonReport)))
            {
                BsonClassMap.RegisterClassMap<ComprehensivePersonReport>(map =>
                {
                    map.AutoMap();
                    map.MapMember(m => m.ReportDate).SetSerializer(new DateTimeOffsetSerializer(BsonType.Document));
                    map.MapMember(m => m.ExpirationDate).SetSerializer(new DateTimeOffsetSerializer(BsonType.Document));
                    map.MapMember(m => m.Report).SetSerializer(new BsonEncryptor<object, object>(encryptionService));
                    var type = typeof(ComprehensivePersonReport);
                    map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                    map.SetIsRootClass(true);
                });
            }
            CreateIndexIfNotExists("bl-credit-report", Builders<IComprehensivePersonReport>.IndexKeys.Ascending(i => i.TenantId).Ascending(i => i.EntityType).Ascending(i => i.EntityId).Ascending(i => i.SSN).Ascending(i => i.FirstName).Ascending(i => i.ExpirationDate));
            TenantTime = tenantTime;
        }

        private ITenantTime TenantTime { get; }

        public async Task<IComprehensivePersonReport> Get(string entityType, string entityId, string ssn, string firstName)
        {
                if (string.IsNullOrWhiteSpace(ssn))
                    throw new ArgumentException($"#{nameof(ssn)} cannot be null");
                if (string.IsNullOrWhiteSpace(firstName))
                    throw new ArgumentException($"#{nameof(firstName)} cannot be null");
                var now = TenantTime.Now;
                return await Query
                .FirstOrDefaultAsync(p => p.EntityType == entityType && p.EntityId == entityId && p.SSN == ssn && p.FirstName == firstName && p.ExpirationDate >=now && p.FirstName == firstName);
        }

        public async Task<IComprehensivePersonReport> GetReport(string entityType, string entityId, string ssn, string reportId)
        {
                if (string.IsNullOrWhiteSpace(ssn))
                    throw new ArgumentException($"#{nameof(ssn)} cannot be null");
                if (string.IsNullOrWhiteSpace(reportId))
                    throw new ArgumentException($"#{nameof(reportId)} cannot be null");
            return await Query
            .FirstOrDefaultAsync(p => p.EntityType == entityType && p.EntityId == entityId && p.SSN == ssn && p.Id == reportId);
        }
    }
}