﻿using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Security.Encryption;
using LendFoundry.Tenant.Client;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Serializers;
using MongoDB.Driver;
using MongoDB.Driver.Linq;
using System;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.Tloxp.Persistence
{
    public class ComprehensiveBusinessReportRepository : MongoRepository<IComprehensiveBusinessReport, ComprehensiveBusinessReport>, IComprehensiveBusinessReportRepository
    {
        public ComprehensiveBusinessReportRepository(IMongoConfiguration mongoConfiguration, ITenantService tenantService, ITenantTime tenantTime, IEncryptionService encryptionService)
             : base(tenantService, mongoConfiguration, "comprehensive-business")
        {
            if (!BsonClassMap.IsClassMapRegistered(typeof(ComprehensiveBusinessReport)))
            {
                BsonClassMap.RegisterClassMap<ComprehensiveBusinessReport>(map =>
                {
                    map.AutoMap();
                    map.MapMember(m => m.ReportDate).SetSerializer(new DateTimeOffsetSerializer(BsonType.Document));
                    map.MapMember(m => m.ExpirationDate).SetSerializer(new DateTimeOffsetSerializer(BsonType.Document));
                    map.MapMember(m => m.Report).SetSerializer(new BsonEncryptor<object, object>(encryptionService));
                    var type = typeof(ComprehensiveBusinessReport);
                    map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                    map.SetIsRootClass(true);
                });
            }
            CreateIndexIfNotExists("tloxp", Builders<IComprehensiveBusinessReport>.IndexKeys.Ascending(i => i.TenantId).Ascending(i => i.EntityType).Ascending(i => i.EntityId).Ascending(i => i.FeinNumber).Ascending(i => i.DunsNumber).Ascending(i => i.ExpirationDate));
            TenantTime = tenantTime;
        }

        private ITenantTime TenantTime { get; }

        public async Task<IComprehensiveBusinessReport> Get(string entityType, string entityId, string feinno)
        {
            if (string.IsNullOrWhiteSpace(feinno))
                throw new ArgumentException($"#{nameof(feinno)} cannot be null");
            var now = TenantTime.Now;
            return await
                 Query.FirstOrDefaultAsync(p => p.EntityType == entityType && p.EntityId == entityId && p.FeinNumber == feinno && p.ExpirationDate >= now);
        }

        public async Task<IComprehensiveBusinessReport> GetReport(string entityType, string entityId, string feinno, string reportId)
        {
            if (string.IsNullOrWhiteSpace(feinno))
                throw new ArgumentException($"#{nameof(feinno)} cannot be null");
            if (string.IsNullOrWhiteSpace(reportId))
                throw new ArgumentException($"#{nameof(reportId)} cannot be null");
            return await Query
                 .FirstOrDefaultAsync(p => p.EntityType == entityType && p.EntityId == entityId && p.FeinNumber == feinno && p.Id == reportId);
        }
    }
}