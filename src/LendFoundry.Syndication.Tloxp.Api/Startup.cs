﻿using LendFoundry.Configuration.Client;
using LendFoundry.Configuration;
using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Lookup.Client;
using LendFoundry.Foundation.Services;
using LendFoundry.Security.Tokens;
using LendFoundry.Tenant.Client;
using LendFoundry.Foundation.ServiceDependencyResolver;
using LendFoundry.Syndication.Tloxp.Proxy;
using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Syndication.Tloxp.Persistence;
using LendFoundry.Security.Encryption;
using System.IO;
using Microsoft.AspNetCore.Http;
using System.ServiceModel;
using SoapCore;
#if DOTNET2
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Swashbuckle.AspNetCore.Swagger;
using Microsoft.Extensions.PlatformAbstractions;
#else
using Microsoft.Framework.DependencyInjection;
using LendFoundry.Foundation.Documentation;
using Microsoft.AspNet.Builder;
using Microsoft.AspNet.Hosting;
#endif
namespace LendFoundry.Syndication.Tloxp.Api
{
    internal class Startup
    {
        #region Public Methods

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        { // Enable middleware to serve generated Swagger as a JSON endpoint.
            app.UseHealthCheck();
		app.UseCors(env);

#if DOTNET2
            app.UseSwagger();

            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/docs/swagger.json", "Tloxp Service");
            });
#else
            app.UseSwaggerDocumentation();
#endif
            
            app.UseErrorHandling();
            app.UseRequestLogging();
            app.UseMvc();
        }

        // This method gets called by the runtime. Use this method to add services to the container
        public void ConfigureServices(IServiceCollection services)
        {
            // Register the Swagger generator, defining one or more Swagger documents
#if DOTNET2

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("docs", new Info
                {
                    Version = PlatformServices.Default.Application.ApplicationVersion,
                    Title = "Tloxp"
                });
                c.AddSecurityDefinition("apiKey", new ApiKeyScheme()
                {
                    Type = "apiKey",
                    Name = "Authorization",
                    Description = "For accessing the API a valid JWT token must be passed in all the queries in the 'Authorization' header. The syntax used in the 'Authorization' header should be Bearer xxxxx.yyyyyy.zzzz",
                    In = "header"
                });
                c.DescribeAllEnumsAsStrings();
                c.IgnoreObsoleteProperties();
                c.DescribeStringEnumsInCamelCase();
                c.IgnoreObsoleteActions();
                var basePath = PlatformServices.Default.Application.ApplicationBasePath;
                var xmlPath = Path.Combine(basePath, "LendFoundry.Syndication.Tloxp.Api.xml");
                c.IncludeXmlComments(xmlPath);
            });
            services.TryAddSingleton<IHttpContextAccessor, HttpContextAccessor>();
#else
            services.AddSwaggerDocumentation();
#endif
            // services
            services.AddTokenHandler();
            services.AddHttpServiceLogging(Settings.ServiceName);
            services.AddTenantTime();

            // aspnet mvc related
            services.AddMvc().AddLendFoundryJsonOptions();
            services.AddCors();

            // interface implements
            services.AddConfigurationService<TloxpConfiguration>(Settings.ServiceName);
            services.AddEventHub(Settings.ServiceName);
            services.AddTenantService();
            services.AddDependencyServiceUriResolver<TloxpConfiguration>(Settings.ServiceName);
            services.AddMongoConfiguration(Settings.ServiceName);
            //services.AddSingleton<IMongoConfiguration>(p => new MongoConfiguration(Settings.Mongo.ConnectionString, Settings.Mongo.Database));
            services.AddTransient<IComprehensiveBusinessReportRepository, ComprehensiveBusinessReportRepository>();
            services.AddTransient<IComprehensivePersonReportRepository, ComprehensivePersonReportRepository>();
            services.AddEncryptionHandler();

            // Configuration factory
            services.AddTransient<ITloxpConfiguration>(p =>
            {
                var configuration = p.GetService<IConfigurationService<TloxpConfiguration>>().Get();
                //   configuration.ProxyUrl = $"http://{Settings.TlsProxy.Host}:{Settings.TlsProxy.Port}";
                return configuration;
            });
            services.AddLookupService();

            services.AddTransient<ITloxpProxy, TloxpProxy>();
            services.AddTransient<ITloxpService, TloxpService>();
             services.AddDependencyServiceUriResolver<TloxpConfiguration>(Settings.ServiceName);
        }

        #endregion Public Methods
    }
}
