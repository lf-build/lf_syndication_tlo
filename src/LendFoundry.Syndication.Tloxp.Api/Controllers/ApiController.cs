﻿using LendFoundry.Foundation.Services;
using System.Threading.Tasks;
#if DOTNET2
using Microsoft.AspNetCore.Mvc;
#else
using Microsoft.AspNet.Mvc;
#endif


namespace LendFoundry.Syndication.Tloxp.Api.Controllers
{
  /// <summary>
  /// ApiController
  /// </summary>
  [Route("/")]
  public class ApiController : ExtendedController
  {
    /// <summary>
    /// ApiController constructor
    /// </summary>
    /// <param name="service"></param>
    public ApiController(ITloxpService service)
    {
      Service = service;
    }

    private ITloxpService Service { get; }

    /// <summary>
    /// Creates comprehensive person report.
    /// </summary>
    /// <param name="entityType"></param>
    /// <param name="entityId"></param>
    /// <param name="creditReportRequest"></param>
    /// <returns></returns>
    [HttpPost("{entityType}/{entityId}/person")]
    public Task<IActionResult> CreateComprehensivePersonReport(string entityType, string entityId, [FromBody]GetComprehensivePersonRequest creditReportRequest)
    {
      return ExecuteAsync(async () => Ok(await Service.CreateComprehensivePersonReport(entityType, entityId, creditReportRequest)));
    }

    /// <summary>
    /// Create comprehensive business report.
    /// </summary>
    /// <param name="entityType"></param>
    /// <param name="entityId"></param>
    /// <param name="creditReportRequest"></param>
    /// <returns></returns>
    [HttpPost("{entityType}/{entityId}/business")]
    public Task<IActionResult> CreateComprehensiveBusinessReport(string entityType, string entityId, [FromBody] GetBusinessReportRequest creditReportRequest)
    {
      return ExecuteAsync(async () => Ok(await Service.CreateComprehensiveBusinessReport(entityType, entityId, creditReportRequest)));
    }

    /// <summary>
    /// Create Business Report.
    /// </summary>
    /// <param name="entityType"></param>
    /// <param name="entityId"></param>
    /// <param name="token"></param>
    /// <returns></returns>
    [HttpPost("{entityType}/{entityId}/business/report")]
    public Task<IActionResult> ComprehensiveBusinessReportBasedOnToken(string entityType, string entityId, [FromBody]GetComprehensiveBusinessRequest token)
    {
      return ExecuteAsync(async () => Ok(await Service.ComprehensiveBusinessReportBasedOnToken(entityType, entityId, token)));
    }

    /// <summary>
    /// Search Businesses.
    /// </summary>
    /// <param name="entityType"></param>
    /// <param name="entityId"></param>
    /// <param name="businessSearchRequest"></param>
    /// <returns></returns>
    [HttpPost("{entityType}/{entityId}/business/search")]
    public Task<IActionResult> ComprehensiveBusinessSearch(string entityType, string entityId, [FromBody]GetBusinessReportRequest businessSearchRequest)
    {
      return ExecuteAsync(async () => Ok(await Service.ComprehensiveBusinessSearch(entityType, entityId, businessSearchRequest)));
    }

    /// <summary>
    /// Get comprehensive business report.
    /// </summary>
    /// <param name="entityType">Type of the entity.</param>
    /// <param name="entityId">The entity identifier.</param>
    /// <param name="feinno">The feinno.</param>
    /// <param name="reportId">The report identifier.</param>
    /// <returns></returns>
    [HttpGet("/business/{entityType}/{entityId}/{feinno}/{reportId}")]
    public Task<IActionResult> GetComprehensiveBusinessReport(string entityType, string entityId, string feinno, string reportId)
    {
      return ExecuteAsync(async () => Ok(await Service.GetComprehensiveBusinessReport(entityType, entityId, feinno, reportId)));
    }

    /// <summary>
    /// Get comprehensive business reports.
    /// </summary>
    /// <param name="entityType">Type of the entity.</param>
    /// <param name="entityId">The entity identifier.</param>
    /// <param name="feinno">The feinno.</param>
    /// <param name="skip">The skip.</param>
    /// <param name="take">The take.</param>
    /// <returns></returns>
    [HttpGet("/businessreports/{entityType}/{entityId}/{feinno}/{skip?}/{take?}")]
    public Task<IActionResult> GetComprehensiveBusinessReports(string entityType, string entityId, string feinno, int? skip = default(int?), int? take = default(int?))
    {
      return ExecuteAsync(async () => Ok(await Service.GetComprehensiveBusinessReports(entityType, entityId, feinno, skip, take)));
    }

    /// <summary>
    /// Get comprehensive person report.
    /// </summary>
    /// <param name="entityType">Type of the entity.</param>
    /// <param name="entityId">The entity identifier.</param>
    /// <param name="ssn">The SSN.</param>
    /// <param name="reportId">The report identifier.</param>
    /// <returns></returns>
    [HttpGet("/person/{entityType}/{entityId}/{ssn}/{reportId}")]
    public Task<IActionResult> GetComprehensivePersonReport(string entityType, string entityId, string ssn, string reportId)
    {
      return ExecuteAsync(async () => Ok(await Service.GetComprehensivePersonReport(entityType, entityId, ssn, reportId)));
    }

    /// <summary>
    /// Get comprehensive person reports.
    /// </summary>
    /// <param name="entityType">Type of the entity.</param>
    /// <param name="entityId">The entity identifier.</param>
    /// <param name="ssn">The SSN.</param>
    /// <param name="skip">The skip.</param>
    /// <param name="take">The take.</param>
    /// <returns></returns>
    [HttpGet("/personreports/{entityType}/{entityId}/{ssn}/{skip?}/{take?}")]
    public Task<IActionResult> GetComprehensivePersonReports(string entityType, string entityId, string ssn, int? skip = default(int?), int? take = default(int?))
    {
      return ExecuteAsync(async () => Ok(await Service.GetComprehensivePersonReports(entityType, entityId, ssn, skip, take)));
    }
  }
}