﻿using LendFoundry.Syndication.Tloxp.CreditReportRequest.AddressBusinessPerson;
using System;

namespace LendFoundry.Syndication.Tloxp
{
  public class GetComprehensivePersonRequest : IGetComprehensivePersonRequest
  {
    public string NameSuffix { get; set; }
    public string FirstName { get; set; }
    public string MiddleName { get; set; }
    public string LastName { get; set; }
    public string SSN { get; set; }
    public IAddressRequest Address { get; set; }
    public string Phone { get; set; }
    public DateTime? DateOfBirth { get; set; }
  }
}