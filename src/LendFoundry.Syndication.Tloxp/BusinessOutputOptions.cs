﻿namespace LendFoundry.Syndication.Tloxp
{
    public class BusinessOutputOptions : IBusinessOutputOptions
    {
        public string ShowBankruptcies { get; set; } = "1";
        public string ShowJudgements { get; set; } = "1";
        public string ShowLiens { get; set; } = "1";
        public string ShowPropertyForeclosures { get; set; } = "1";
        public string ShowUCCFilings { get; set; } = "1";
        public string ShowCorporateFilings { get; set; } = "1";
    }
}