﻿namespace LendFoundry.Syndication.Tloxp.CreditReportResponse.ComprehensivePerson
{
    public class SsnRecord : ISsnRecord
    {
        public SsnRecord(Proxy.SSNRecord response)
        {
            Ssn = response.SSN;
            First5Masked = response.First5Masked;
            Last4Masked = response.Last4Masked;
        }

        public string Ssn { get; set; }
        public string First5Masked { get; set; }
        public string Last4Masked { get; set; }
    }
}