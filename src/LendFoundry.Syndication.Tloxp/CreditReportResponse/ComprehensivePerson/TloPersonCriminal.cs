﻿using LendFoundry.Syndication.Tloxp.Proxy;
using System.Collections.Generic;
using System;

namespace LendFoundry.Syndication.Tloxp.CreditReportResponse.ComprehensivePerson
{
    public class TloPersonCriminal : ITloPersonCriminal
    {
        public TloPersonCriminal(TLOCriminalSearchMatch tLOCriminalSearchMatch)
        {
            if (tLOCriminalSearchMatch.CriminalRecord != null)
            {
                if (tLOCriminalSearchMatch?.CriminalRecord.CrimeDetails != null)
                {
                    CrimeDetails = new List<ICrimeDetails>();
                    foreach (var criminalrecord in tLOCriminalSearchMatch.CriminalRecord.CrimeDetails)
                    {
                        CrimeDetails.Add(new CrimeDetails(criminalrecord));
                    }
                }
                if (tLOCriminalSearchMatch?.CriminalRecord.WarrantDetails != null)
                {
                    WarrantDetails = new List<IWarrantDetails>();
                    foreach (var arrestrecord in tLOCriminalSearchMatch.CriminalRecord.WarrantDetails)
                    {
                        WarrantDetails.Add(new WarrantDetails(arrestrecord));
                    }
                }
            }
        }
        public List<ICrimeDetails> CrimeDetails { get; set; }
        public List<IWarrantDetails> WarrantDetails { get; set; }
    }
}