﻿using LendFoundry.Syndication.Tloxp.Proxy;

namespace LendFoundry.Syndication.Tloxp.CreditReportResponse.ComprehensivePerson
{
    public class TloJudgmentFiling : ITloJudgmentFiling
    {
        public TloJudgmentFiling(TLOPersonalJudgmentFiling judgment)
        {
            if (judgment != null)
            {
                if (judgment.FilingDate != null)
                {
                    FilingDate = new Date(judgment.FilingDate.Day, judgment.FilingDate.Month, judgment.FilingDate.Year);
                }
                FilingType = judgment.FilingType;
                TotalJudgmentAmount = judgment.TotalJudgmentAmount;
            }
        }

        public string FilingType { get; set; }
        public IDate FilingDate { get; set; }
        public string TotalJudgmentAmount { get; set; }
    }
}