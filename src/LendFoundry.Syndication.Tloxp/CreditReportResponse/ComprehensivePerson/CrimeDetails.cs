﻿using LendFoundry.Syndication.Tloxp.Proxy;
using System.Collections.Generic;

namespace LendFoundry.Syndication.Tloxp.CreditReportResponse.ComprehensivePerson
{
    public class CrimeDetails : ICrimeDetails
    {
        public CrimeDetails(CrimeDetails1 criminalrecord)
        {
            if (criminalrecord != null)
            {
                CaseNumber = criminalrecord.CaseNumber;
                if (criminalrecord.DispositionDate != null)
                {
                    DispositionDate = new Date(criminalrecord.DispositionDate.Day, criminalrecord.DispositionDate.Month, criminalrecord.DispositionDate.Year);
                }
                OffenceCode = criminalrecord.OffenseCode;
                Disposition = criminalrecord.Disposition;
                Statute = criminalrecord.Statute;
                Plea = criminalrecord.Plea;
                if (criminalrecord.ChargesFiledDate != null)
                {
                    FilingDate = new Date(criminalrecord.ChargesFiledDate.Day, criminalrecord.ChargesFiledDate.Month, criminalrecord.ChargesFiledDate.Year);
                }
            }
        }
        public string CaseNumber { get; set; }
        public string OffenceCode { get; set; }
        public IDate DispositionDate { get; set; }
        public string Disposition { get; set; }
        public string Statute { get; set; }
        public string Plea { get; set; }
        public IDate FilingDate { get; set; }
    }
}