﻿using LendFoundry.Syndication.Tloxp.Proxy;
using System.Collections.Generic;

namespace LendFoundry.Syndication.Tloxp.CreditReportResponse.ComprehensivePerson
{
    public class TloAddressRequest : ITloAddressRequest, IAddress
    {
        // private Proxy.Address mailingAddress;

        public TloAddressRequest(Proxy.Address addressreq)
        {
            if (addressreq != null)
            {
                Line1 = addressreq.Line1;
                Line2 = addressreq.Line2;
                Line3 = addressreq.Line3;
                City = addressreq.City;
                State = addressreq.State;
                Zip = addressreq.Zip;
                County = addressreq.County;
               
            }
        }

        public string Line1 { get; set; }
        public string Line2 { get; set; }
        public string Line3 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string County { get; set; }
      }
}