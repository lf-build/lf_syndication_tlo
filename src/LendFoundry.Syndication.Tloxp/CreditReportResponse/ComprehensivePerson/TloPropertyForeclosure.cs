﻿using LendFoundry.Syndication.Tloxp.Proxy;
using System.Collections.Generic;

namespace LendFoundry.Syndication.Tloxp.CreditReportResponse.ComprehensivePerson
{
    public class TloPropertyForeclosure : ITloPropertyForeclosure
    {
        public TloPropertyForeclosure(PropertyForeclosure closure)
        {
            if (closure != null) {
                FIPSCounty=closure.FIPSCounty;
                AmountOfDefault = closure.AmountOfDefault;
                FinalJudgmentAmount = closure.FinalJudgmentAmount;
                CourtCaseNumber = closure.CourtCaseNumber;
                if (closure.FilingDate != null)
                {
                    FilingDate = new Date(closure.FilingDate.Day,closure.FilingDate.Month,closure.FilingDate.Year);
                }
                if (closure?.Owners != null)
                {
                    Owners = new List<ITloName>();
                    foreach (var name in closure.Owners)
                    {
                        Owners.Add(new TloName(name));
                    }
                }
                if (closure.Address != null)
                {
                    Address = new TloAddressRequest(closure.Address);
                }
                if (closure.MailingAddress != null)
                {
                    MailingAddress = new TloAddressRequest(closure.MailingAddress);
                }
                if (closure.AuctionCallAddress != null)
                {
                    AuctionCallAddress = new TloAddressRequest(closure.AuctionCallAddress);
                }
                if (closure.Lender != null)
                {
                    Lender = new TloName(closure.Lender);
                }
                if (closure.LenderAddress != null)
                {
                    LenderAddress = new TloAddressRequest(closure.LenderAddress);
                }
                if (closure.TrusteeAddress != null)
                {
                    TrusteeAddress = new TloAddressRequest(closure.TrusteeAddress);
                }
            }
        }
        public string FIPSCounty{ get; set; }
        public IDate FilingDate{ get; set; }
        public string AmountOfDefault{ get; set; }
        public string FinalJudgmentAmount{ get; set; }
        public string CourtCaseNumber{ get; set; }
        public List<ITloName> Owners { get; set; }

        public ITloAddressRequest Address { get; set; }

        public ITloAddressRequest MailingAddress { get; set; }

        public ITloAddressRequest AuctionCallAddress { get; set; }

        public ITloName Lender { get; set; }

        public ITloAddressRequest LenderAddress { get; set; }

        public ITloAddressRequest TrusteeAddress { get; set; }
    }
}