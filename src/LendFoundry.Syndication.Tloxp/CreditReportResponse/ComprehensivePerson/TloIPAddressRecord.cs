﻿using LendFoundry.Syndication.Tloxp.Proxy;

namespace LendFoundry.Syndication.Tloxp.CreditReportResponse.ComprehensivePerson
{
    public class TloIPAddressRecord : ITloIPAddressRecord
    {
        public TloIPAddressRecord(IPAddressRecord ipaddressrecord)
        {
            IPAddress = ipaddressrecord.IPAddress;
            Country = ipaddressrecord.Country;
            Region = ipaddressrecord.Region;
            City = ipaddressrecord.City;
            PostalCode = ipaddressrecord.PostalCode;
            ISP = ipaddressrecord.ISP;
            BusinessName = ipaddressrecord.BusinessName;
            DomainName = ipaddressrecord.DomainName;
            Latitude = ipaddressrecord.Latitude;
            Longitude = ipaddressrecord.Longitude;
            if (ipaddressrecord.DateFirstSeen != null)
            {
                DateFirstSeen = new Date(ipaddressrecord.DateFirstSeen.Day, ipaddressrecord.DateFirstSeen.Month, ipaddressrecord.DateFirstSeen.Year);
            }
            if (ipaddressrecord.DateLastSeen != null)
            {
                DateLastSeen = new Date(ipaddressrecord.DateLastSeen.Day, ipaddressrecord.DateLastSeen.Month, ipaddressrecord.DateLastSeen.Year);
            }
            if (ipaddressrecord.TimeFirstSeen != null)
            {
                TimeFirstSeen = new TloTime(ipaddressrecord.TimeFirstSeen);
            }
            if (ipaddressrecord.TimeLastSeen != null)
            {
                TimeLastSeen = new TloTime(ipaddressrecord.TimeLastSeen);
            }
        }

        public string IPAddress { get; set; }

        public string Country { get; set; }

        public string Region { get; set; }

        public string City { get; set; }

        public string PostalCode { get; set; }

        public string ISP { get; set; }

        public string BusinessName { get; set; }

        public string DomainName { get; set; }

        public string Latitude { get; set; }

        public string Longitude { get; set; }

        public IDate DateFirstSeen { get; set; }

        public ITloTime TimeFirstSeen { get; set; }

        public IDate DateLastSeen { get; set; }

        public ITloTime TimeLastSeen { get; set; }
    }
}