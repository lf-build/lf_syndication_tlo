﻿using LendFoundry.Syndication.Tloxp.Proxy;
using System.Collections.Generic;

namespace LendFoundry.Syndication.Tloxp.CreditReportResponse.ComprehensivePerson
{
    public class ComprehensivePersonRecord : IComprehensivePersonRecord
    {
        public ComprehensivePersonRecord(TLOComprehensivePersonSearchOutput response)
        {
            if (response != null)
            {
                ReportToken = response.ReportToken;
                if (response.Names != null)
                {
                    Names = new List<IPersonName>();
                    foreach (var name in response.Names)
                    {
                        Names.Add(new PersonName(name));
                    }
                }

                if (response.SSNRecords != null)
                {
                    SsnRecords = new List<ISsnRecord>();
                    foreach (var ssnRecord in response.SSNRecords)
                    {
                        SsnRecords.Add(new SsnRecord(ssnRecord));
                    }
                }
                if (response.DatesOfBirth != null)
                {
                    DatesOfBirth = new List<IDate>();
                    foreach (var dateOfBirth in response.DatesOfBirth)
                    {
                        if (dateOfBirth.DateOfBirth != null)
                            DatesOfBirth.Add(new Date(dateOfBirth.DateOfBirth.Day, dateOfBirth.DateOfBirth.Month, dateOfBirth.DateOfBirth.Year));
                    }
                }
                if (response.DatesOfDeath != null)
                {
                    DatesOfDeath = new List<IDate>();
                    foreach (var dateOfDeath in response.DatesOfDeath)
                    {
                        if (dateOfDeath.DateOfDeath != null)
                            DatesOfDeath.Add(new Date(dateOfDeath.DateOfDeath.Day, dateOfDeath.DateOfDeath.Month, dateOfDeath.DateOfDeath.Year));
                    }
                }

                if (response?.Addresses != null)
                {
                    Addresses = new List<IAddress>();
                    foreach (var address in response.Addresses)
                    {
                        if (address.Address != null)
                            Addresses.Add(new Address(address.Address));
                    }
                }
            }
        }

        public string ReportToken { get; set; }
        public List<IPersonName> Names { get; set; }

        public List<ISsnRecord> SsnRecords { get; set; }

        public List<IDate> DatesOfBirth { get; set; }

        public List<IDate> DatesOfDeath { get; set; }

        public List<IAddress> Addresses { get; set; }
    }
}