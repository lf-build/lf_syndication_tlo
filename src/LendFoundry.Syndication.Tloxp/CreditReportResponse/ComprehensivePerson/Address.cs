﻿namespace LendFoundry.Syndication.Tloxp.CreditReportResponse.ComprehensivePerson
{
    public class Address : IAddress
    {
        public Address(Proxy.BasicAddress response)
        {
            Line1 = response.Line1;
            Line2 = response.Line2;
            Line3 = response.Line3;
            City = response.City;

            State = response.State;
            Zip = response.Zip;
            County = response.County;
        }

        public string Line1 { get; set; }
        public string Line2 { get; set; }

        public string Line3 { get; set; }
        public string City { get; set; }
        public string State { get; set; }

        public string Zip { get; set; }
        public string County { get; set; }
    }
}