﻿using LendFoundry.Syndication.Tloxp.Proxy;

namespace LendFoundry.Syndication.Tloxp.CreditReportResponse.ComprehensivePerson
{
    public class TloPersonBankruptcy : ITloPersonBankruptcy
    {
        public TloPersonBankruptcy(TLOBankruptcy tLOBankruptcy)
        {
            if (tLOBankruptcy != null)
            {
                if (tLOBankruptcy.FileDate != null)
                {
                    FilingDate = new Date(tLOBankruptcy.FileDate.Day, tLOBankruptcy.FileDate.Month, tLOBankruptcy.FileDate.Year);
                }
                if (tLOBankruptcy.DischargeDate != null)
                {
                    DischargeDate = new Date(tLOBankruptcy.DischargeDate.Day, tLOBankruptcy.DischargeDate.Month, tLOBankruptcy.DischargeDate.Year);
                }
                Chapter = tLOBankruptcy.Chapter;
            }
        }
        public IDate FilingDate { get; set; }

        public IDate DischargeDate { get; set; }

        public string Chapter { get; set; }
    }
}