﻿using LendFoundry.Syndication.Tloxp.Proxy;

namespace LendFoundry.Syndication.Tloxp.CreditReportResponse.ComprehensivePerson
{
    public class TloName : ITloName, IPersonName
    {
        public TloName(Name response)
        {
            NameSuffix = response.NameSuffix;
            FirstName = response.FirstName;
            MiddleName = response.MiddleName;
            LastName = response.LastName;
            Observations = response.Observations;
            Score = response.Score;
            Display = response.Display;
            Source = response.Score;
        }

        public string Observations { get; set; }

        public string Score { get; set; }

        public string Display { get; set; }

        public string Source { get; set; }
        public string NameSuffix { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
    }
}