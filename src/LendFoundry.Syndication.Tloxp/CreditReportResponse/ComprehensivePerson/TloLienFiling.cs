﻿using LendFoundry.Syndication.Tloxp.Proxy;

namespace LendFoundry.Syndication.Tloxp.CreditReportResponse.ComprehensivePerson
{
    public class TloLienFiling : ITloLienFiling
    {
        public TloLienFiling(TLOPersonalLienFiling lien)
        {
            if (lien != null)
            {
                FilingType = lien.FilingType;
                TotalLienAmount = lien.TotalLienAmount;
                FederalTaxLienArea = lien.FederalTaxLienArea;
                if (lien.FilingDate != null)
                {
                    FilingDate = new Date(lien.FilingDate.Day, lien.FilingDate.Month, lien.FilingDate.Year);
                }
                if (lien.TaxLienDate != null)
                {
                    TaxLienDate = new Date(lien.TaxLienDate.Day, lien.TaxLienDate.Month, lien.TaxLienDate.Year);
                }
            }
        }

        public string FilingType { get; set; }
        public string TotalLienAmount { get; set; }
        public IDate FilingDate { get; set; }
        public IDate TaxLienDate { get; set; }
        public string FederalTaxLienArea { get; set; }
    }
}