﻿using LendFoundry.Syndication.Tloxp.Proxy;
using System.Collections.Generic;

namespace LendFoundry.Syndication.Tloxp.CreditReportResponse.ComprehensivePerson
{
    public class WarrantDetails : IWarrantDetails
    {
        public WarrantDetails(WarrantDetails1 arrestrecord)
        {
            if (arrestrecord != null)
            {
                OffenceType = arrestrecord.OffenseType;
                CaseNumber = arrestrecord.CaseNumber;
            }
        }
        public string OffenceType { get; set; }
        public string CaseNumber { get; set; }
    }
}