﻿using LendFoundry.Syndication.Tloxp.Proxy;

namespace LendFoundry.Syndication.Tloxp.CreditReportResponse.ComprehensivePerson
{
    public class TloTime : ITloTime
    {
        public TloTime(Time time)
        {
            if (time != null)
            {
                Hour = time.Hour;
                Minute = time.Minute;
                Second = time.Second;
            }
        }

        public string Hour { get; set; }

        public string Minute { get; set; }

        public string Second { get; set; }
    }
}