﻿namespace LendFoundry.Syndication.Tloxp.CreditReportResponse.ComprehensivePerson
{
    public class PersonName : IPersonName
    {
        public PersonName(Proxy.BasicName response)
        {
            NameSuffix = response.NameSuffix;
            FirstName = response.FirstName;
            MiddleName = response.MiddleName;
            LastName = response.LastName;
        }

        public string NameSuffix { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
    }
}