﻿using LendFoundry.Syndication.Tloxp.Proxy;

namespace LendFoundry.Syndication.Tloxp.CreditReportResponse.ComprehensivePerson
{
    public class TloUccFiling : ITloUccFiling
    {
        public TloUccFiling(UCCFiling ucc)
        {
            if (ucc != null)
            {
                FilingType = ucc.FilingType;
                FilingNumber = ucc.FilingNumber;
                FilingOfficeName = ucc.FilingOfficeName;
                if (ucc.FilingDate != null)
                {
                    FilingDate = new Date(ucc.FilingDate.Day, ucc.FilingDate.Month, ucc.FilingDate.Year);
                }
            }
        }

        public string FilingType { get; set; }
        public string FilingNumber { get; set; }
        public IDate FilingDate { get; set; }
        public string FilingOfficeName { get; set; }
    }
}