﻿using LendFoundry.Syndication.Tloxp.Proxy;

namespace LendFoundry.Syndication.Tloxp.CreditReportResponse.ComprehensiveBusiness
{
    public class TloLienFiling : ITloLienFiling
    {
        public TloLienFiling(TLOLienFiling lien)
        {
            if (lien != null) { 
            if (lien?.FilingDate != null)
            {
                FilingDate = new Date(lien.FilingDate.Day, lien.FilingDate.Month, lien.FilingDate.Year);
            }
            LienAmount = lien.LienAmount;
            LienType = lien.LienType;
            }
        }

        public IDate FilingDate { get; set; }
        public string LienAmount { get; set; }
        public string LienType { get; set; }
    }
}