﻿using LendFoundry.Syndication.Tloxp.Proxy;

namespace LendFoundry.Syndication.Tloxp.CreditReportResponse.ComprehensiveBusiness
{
    public class TloBasicDateOfBirthRecord : ITloBasicDateOfBirthRecord
    {
        public TloBasicDateOfBirthRecord(BasicDateOfBirthRecord dateofbirth)
        {
            if (dateofbirth != null)
            {
                if (dateofbirth.DateOfBirth != null)
                {
                    DateOfBirth = new Date(dateofbirth.DateOfBirth.Day, dateofbirth.DateOfBirth.Month, dateofbirth.DateOfBirth.Year);
                }
                CurrentAge = dateofbirth.CurrentAge;
                Suppressed = dateofbirth.Suppressed;
                YearMasked = dateofbirth.YearMasked;
                MonthMasked = dateofbirth.MonthMasked;
                DayMasked = dateofbirth.DayMasked;
            }
        }

        public IDate DateOfBirth { get; set; }

        public string CurrentAge { get; set; }

        public string Suppressed { get; set; }

        public string YearMasked { get; set; }

        public string MonthMasked { get; set; }

        public string DayMasked { get; set; }
    }
}