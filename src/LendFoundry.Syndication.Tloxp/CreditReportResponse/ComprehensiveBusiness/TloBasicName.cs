﻿using LendFoundry.Syndication.Tloxp.Proxy;

namespace LendFoundry.Syndication.Tloxp.CreditReportResponse.ComprehensiveBusiness
{
    public class TloBasicName : ITloBasicName
    {
        public TloBasicName(BasicName basicname)
        {
            Title = basicname.Title;
            ProfessionalSuffix = basicname.ProfessionalSuffix;
            FirstName = basicname.FirstName;
            MiddleName = basicname.MiddleName;
            LastName = basicname.LastName;
            NameSuffix = basicname.NameSuffix;
            if (basicname.DateFirstSeen != null)
            {
                DateFirstSeen = new Date(basicname.DateFirstSeen.Day, basicname.DateFirstSeen.Month, basicname.DateFirstSeen.Year);
            }
            if (basicname.DateLastSeen != null)
            {
                DateLastSeen = new Date(basicname.DateLastSeen.Day, basicname.DateLastSeen.Month, basicname.DateLastSeen.Year);
            }
        }

        public string Title { get; set; }

        public string ProfessionalSuffix { get; set; }

        public IDate DateFirstSeen { get; set; }

        public IDate DateLastSeen { get; set; }
        public string FirstName { get; set; }

        public string MiddleName { get; set; }

        public string LastName { get; set; }

        public string NameSuffix { get; set; }
    }
}