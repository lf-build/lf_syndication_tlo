﻿namespace LendFoundry.Syndication.Tloxp.CreditReportResponse.ComprehensiveBusiness
{
    public class BusinessName : IBusinessName
    {
        public BusinessName(Proxy.BusinessName response)
        {
            Name = response.Name;
            Type = response.Type;
        }

        public string Name { get; set; }
        public string Type { get; set; }
    }
}