﻿using LendFoundry.Syndication.Tloxp.Proxy;

namespace LendFoundry.Syndication.Tloxp.CreditReportResponse.ComprehensiveBusiness
{
    public class TloBusinessCorporateFiling: ITloBusinessCorporateFiling
    {
        public TloBusinessCorporateFiling(TLOCorporateFiling filing)
        {
            if (filing != null)
            {
                fEINumber = filing.FEINumber;
                sIC = filing.SIC;
            }
        }
        public string fEINumber { get; set; }
        public string sIC { get; set; }

    }
}
