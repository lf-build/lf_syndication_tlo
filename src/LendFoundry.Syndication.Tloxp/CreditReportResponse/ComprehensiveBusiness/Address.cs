﻿using System.Collections.Generic;

namespace LendFoundry.Syndication.Tloxp.CreditReportResponse.ComprehensiveBusiness
{
    public class Address : IAddress
    {
        public Address(Proxy.BasicAddress response)
        {
            Line1 = response.Line1;
            Line2 = response.Line2;
            Line3 = response.Line3;
            City = response.City;
            State = response.State;
            Zip = response.Zip;
            County = response.County;
            Latitude = response.Latitude;
            Longitude = response.Longitude;
            Zip4 = response.Zip4;
            CountryName = response.CountryName;
            BuildingName = response.BuildingName;
            Description = response.Description;
            SubdivisionName = response.SubdivisionName;
            Distance = response.Distance;
            ReportToken = response.ReportToken;
            AddressMissingUnitDesignation = response.AddressMissingUnitDesignation;
            Link = response.Link;
            PropertyPhotosAddress = response.PropertyPhotosAddress;
            if (response.DateFirstSeen != null)
            {
                DateFirstSeen = new Date(response.DateFirstSeen.Day, response.DateFirstSeen.Month, response.DateFirstSeen.Year);
            }
            if (response.DateLastSeen != null)
            {
                DateLastSeen = new Date(response.DateLastSeen.Day, response.DateLastSeen.Month, response.DateLastSeen.Year);
            }
            if (response?.PropertyPhotoURLs != null)
            {
                PropertyPhotoURLs = new List<string>();
                foreach (var url in response.PropertyPhotoURLs)
                {
                    PropertyPhotoURLs.Add(url);
                }
            }
        }

        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public string Zip4 { get; set; }
        public string CountryName { get; set; }

        public IDate DateFirstSeen { get; set; }

        public IDate DateLastSeen { get; set; }

        public string BuildingName { get; set; }

        public string Description { get; set; }

        public string SubdivisionName { get; set; }

        public string Distance { get; set; }

        public string AddressMissingUnitDesignation { get; set; }

        public string ReportToken { get; set; }

        public string Link { get; set; }

        public List<string> PropertyPhotoURLs { get; set; }
        public string PropertyPhotosAddress { get; set; }
        public string Line1 { get; set; }
        public string Line2 { get; set; }

        public string Line3 { get; set; }
        public string City { get; set; }
        public string State { get; set; }

        public string Zip { get; set; }
        public string County { get; set; }
    }
}