﻿using LendFoundry.Syndication.Tloxp.Proxy;

namespace LendFoundry.Syndication.Tloxp.CreditReportResponse.ComprehensiveBusiness
{
    public class TloDebtors : ITloDebtors
    {
        public TloDebtors(TLOUCCDebtor debtor)
        {
            if (debtor != null)
            {
                BusinessName = debtor.BusinessName;
                FEINumber = debtor.FEINumber;
                Name = debtor.Name;
                ReportToken = debtor.ReportToken;
                BusinessToken = debtor.BusinessToken;
                if (debtor.DateOfBirth != null)
                {
                    DateOfBirth = new TloBasicDateOfBirthRecord(debtor.DateOfBirth);
                }
                if (debtor.PreferredSSNRecord != null)
                {
                    PreferredSSNRecord = new TloBasicSSNRecord(debtor.PreferredSSNRecord);
                }
                if (debtor.Address != null)
                {
                    DebtorAddress = new Address(debtor.Address);
                }
            }
        }

        public string BusinessName { get; set; }

        public string FEINumber { get; set; }

        public string Name { get; set; }
        public string ReportToken { get; set; }
        public string BusinessToken { get; set; }
        public ITloBasicDateOfBirthRecord DateOfBirth { get; set; }
        public ITloBasicSSNRecord PreferredSSNRecord { get; set; }
        public IAddress DebtorAddress { get; set; }
    }
}