﻿using LendFoundry.Syndication.Tloxp.Proxy;
using System.Collections.Generic;

namespace LendFoundry.Syndication.Tloxp.CreditReportResponse.ComprehensiveBusiness
{
    public class ComprehensiveBusinessRecord : IComprehensiveBusinessRecord
    {
        public ComprehensiveBusinessRecord(TLOBusinessRecord businessRecord)
        {
            if (businessRecord?.BusinessNames != null)
            {
                BusinessNames = new List<IBusinessName>();
                foreach (var businessName in businessRecord.BusinessNames)

                    BusinessNames.Add(new BusinessName(businessName));
            }
            FeiNumber = businessRecord.FEINumber;
            Industry = businessRecord.Industry;
            if (businessRecord?.Industries != null)
            {
                Industries = new List<string>();
                foreach (var industry in businessRecord.Industries)
                    Industries.Add(industry);
            }
        }

        public List<IBusinessName> BusinessNames { get; set; }

        public string FeiNumber { get; set; }

        public string Industry { get; set; }
        
        public List<string> Industries { get; set; }
    }
}