﻿using LendFoundry.Syndication.Tloxp.Proxy;

namespace LendFoundry.Syndication.Tloxp.CreditReportResponse.ComprehensiveBusiness
{
    public class TloBusinessBankruptcy : ITloBusinessBankruptcy
    {
        public TloBusinessBankruptcy(TLOBankruptcyFiling tLOBankruptcyFiling)
        {
            if (tLOBankruptcyFiling != null) { 
            if (tLOBankruptcyFiling?.FilingDate != null)
            {
                FilingDate = new Date(tLOBankruptcyFiling.FilingDate.Day, tLOBankruptcyFiling.FilingDate.Month, tLOBankruptcyFiling.FilingDate.Year);
            }
            FilingChapterNumber = tLOBankruptcyFiling.FilingNumber;
            Status = tLOBankruptcyFiling.Status;
            if (tLOBankruptcyFiling?.StatusDate != null)
            {
                StatusDate = new Date(tLOBankruptcyFiling.StatusDate.Day, tLOBankruptcyFiling.StatusDate.Month, tLOBankruptcyFiling.StatusDate.Year);
            }
            }
        }
        public IDate FilingDate { get; set; }
        public string FilingChapterNumber { get; set; }
        public string Status { get; set; }
        public IDate StatusDate { get; set; }
    }
}