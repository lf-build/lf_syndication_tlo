﻿namespace LendFoundry.Syndication.Tloxp.CreditReportResponse.ComprehensiveBusiness
{
    public class Date : IDate
    {
        public Date(string day, string month, string year)
        {
            Day = day;
            Month = month;
            Year = year;
        }

        public string Day { get; set; }
        public string Month { get; set; }

        public string Year { get; set; }
    }
}