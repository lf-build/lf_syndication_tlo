﻿using LendFoundry.Syndication.Tloxp.Proxy;

namespace LendFoundry.Syndication.Tloxp.CreditReportResponse.ComprehensiveBusiness
{
    public class TloJudgmentFiling : ITloJudgmentFiling
    {
        public TloJudgmentFiling(TLOJudgmentFiling judgment)
        {
            if (judgment != null)
            {
                if (judgment?.FilingDate != null)
                {
                    FilingDate = new Date(judgment.FilingDate.Day, judgment.FilingDate.Month, judgment.FilingDate.Year);
                }
                JudgmentType = judgment.JudgmentType;
                Status = judgment.Status;
            }
        }

        public IDate FilingDate { get; set; }
        public string JudgmentType { get; set; }
        public string Status { get; set; }
    }
}