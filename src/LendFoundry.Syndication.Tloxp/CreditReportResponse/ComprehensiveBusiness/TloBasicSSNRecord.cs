﻿using LendFoundry.Syndication.Tloxp.Proxy;

namespace LendFoundry.Syndication.Tloxp.CreditReportResponse.ComprehensiveBusiness
{
    public class TloBasicSSNRecord : ITloBasicSSNRecord
    {
        public TloBasicSSNRecord(BasicSSNRecord ssnrecord)
        {
            if (ssnrecord != null)
            {
                SSN = ssnrecord.SSN;
                SSNPlaceOfIssue = ssnrecord.SSNPlaceOfIssue;
                SSNIssueYears = ssnrecord.SSNIssueYears;
                First5Masked = ssnrecord.First5Masked;
                Last4Masked = ssnrecord.Last4Masked;
            }
        }

        public string SSN { get; set; }

        public string SSNPlaceOfIssue { get; set; }

        public string SSNIssueYears { get; set; }

        public string First5Masked { get; set; }

        public string Last4Masked { get; set; }
    }
}