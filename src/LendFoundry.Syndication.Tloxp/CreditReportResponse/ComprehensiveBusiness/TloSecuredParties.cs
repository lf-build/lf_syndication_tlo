﻿using LendFoundry.Syndication.Tloxp.Proxy;

namespace LendFoundry.Syndication.Tloxp.CreditReportResponse.ComprehensiveBusiness
{
    public class TloSecuredParties : ITloSecuredParties
    {
        public TloSecuredParties(TLOUCCSecuredParty uccparty)
        {
            if (uccparty != null)
            {
                if (uccparty.DateOfBirth != null)
                {
                    DateOfBirth = new TloBasicDateOfBirthRecord(uccparty.DateOfBirth);
                }
                if (uccparty.PreferredSSNRecord != null)
                {
                    PreferredSSNRecord = new TloBasicSSNRecord(uccparty.PreferredSSNRecord);
                }
                if (uccparty.Address != null)
                {
                    DebtorAddress = new Address(uccparty.Address);
                }
                Assignee = uccparty.Assignee;
                BusinessName = uccparty.BusinessName;
                FEINumber = uccparty.FEINumber;
                Name = uccparty.Name;
                ReportToken = uccparty.ReportToken;
                BusinessToken = uccparty.BusinessToken;
            }
        }

        public string Assignee { get; set; }
        public string BusinessName { get; set; }
        public string FEINumber { get; set; }
        public string Name { get; set; }
        public string ReportToken { get; set; }
        public string BusinessToken { get; set; }
        public ITloBasicDateOfBirthRecord DateOfBirth { get; set; }
        public ITloBasicSSNRecord PreferredSSNRecord { get; set; }
        public IAddress DebtorAddress { get; set; }
    }
}