﻿using LendFoundry.Syndication.Tloxp.Proxy;
using System.Collections.Generic;

namespace LendFoundry.Syndication.Tloxp.CreditReportResponse.ComprehensiveBusiness
{
    public class TloUCCFiling : ITloUCCFiling
    {
        public TloUCCFiling(TLOUCCFiling ucc)
        {
            if (ucc != null)
            {
                FilingType= ucc.FilingType;
                FilingNumber = ucc.FilingNumber;
                FilingOfficeName= ucc.FilingOfficeName;
                if (ucc?.FilingDate != null)
                {
                    FilingDate = new Date(ucc.FilingDate.Day, ucc.FilingDate.Month, ucc.FilingDate.Year);
                }
                if (ucc?.FilingOfficeAddress != null)
                {
                    FilingOfficeAddress = new Address(ucc.FilingOfficeAddress);
                }
                if (ucc?.Debtors != null)
                {
                    Debtors = new List<ITloDebtors>();
                    foreach (var debtor in ucc.Debtors)
                    {
                        Debtors.Add(new TloDebtors(debtor));
                    }
                }
                if (ucc?.SecuredParties != null)
                {
                    SecurityParty = new List<ITloSecuredParties>();
                    foreach (var party in ucc.SecuredParties)
                    {
                        SecurityParty.Add(new TloSecuredParties(party));
                    }
                }
            }
        }
        public string FilingType { get; set; }
        public string FilingNumber { get; set; }
        public IDate FilingDate { get; set; }
        public string FilingOfficeName { get; set; }

        public IAddress FilingOfficeAddress { get; set; }
        public List<ITloDebtors> Debtors { get; set; }
        public List<ITloSecuredParties> SecurityParty { get; set; }
    }
}