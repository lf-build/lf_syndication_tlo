﻿using LendFoundry.Syndication.Tloxp.Proxy;
using System.Collections.Generic;

namespace LendFoundry.Syndication.Tloxp.CreditReportResponse.ComprehensiveBusiness
{
    public class TloPropertyForeclosure : ITloPropertyForeclosure
    {
        public TloPropertyForeclosure(TLOPropertyForeclosure foreclosure)
        {
            if (foreclosure != null)
            {
                FIPSCounty = foreclosure.FIPSCounty;
                AmountOfDefault = foreclosure.AmountOfDefault;
                FinalJudgmentAmount = foreclosure.FinalJudgmentAmount;
                CourtCaseNumber = foreclosure.CourtCaseNumber;
                if (foreclosure?.FilingDate != null)
                {
                    FilingDate = new Date(foreclosure.FilingDate.Day, foreclosure.FilingDate.Month, foreclosure.FilingDate.Year);
                }
                if (foreclosure?.Owners != null)
                {
                    Owners = new List<ITloBasicName>();
                    foreach (var owner in foreclosure.Owners)
                    {
                        Owners.Add(new TloBasicName(owner));
                    }
                }
                if (foreclosure?.Address != null)
                {
                    var gh = foreclosure.Address.ToString();
                    Address = new Address(foreclosure.Address);
                }
                if (foreclosure?.MailingAddress != null)
                {
                    MailingAddress = new Address(foreclosure.MailingAddress);
                }
                if (foreclosure?.AuctionCallAddress != null)
                {
                    AuctionCallAddress = new Address(foreclosure.AuctionCallAddress);
                }
                if (foreclosure?.Lender != null)
                {
                    Lender = new TloBasicName(foreclosure.Lender);
                }
                if (foreclosure?.LenderAddress != null)
                {
                    LenderAddress = new Address(foreclosure.LenderAddress);
                }
                if (foreclosure?.TrusteeAddress != null)
                {
                    TrusteeAddress = new Address(foreclosure.TrusteeAddress);
                }
            }
        }
        public string FIPSCounty { get; set; }
        public string AmountOfDefault { get; set; }
        public string FinalJudgmentAmount { get; set; }
        public string CourtCaseNumber { get; set; }
        public IDate FilingDate { get; set; }
        public List<ITloBasicName> Owners { get; set; }

        public IAddress Address { get; set; }

        public IAddress MailingAddress { get; set; }

        public IAddress AuctionCallAddress { get; set; }

        public ITloBasicName Lender { get; set; }

        public IAddress LenderAddress { get; set; }

        public IAddress TrusteeAddress { get; set; }
    }
}