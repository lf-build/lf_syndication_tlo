﻿namespace LendFoundry.Syndication.Tloxp.CreditReportResponse.ComprehensiveBusiness
{
    public class BusinessContact : IBusinessContact
    {
        public BusinessContact(Proxy.BusinessContact response)
        {
            BusinessName = response.BusinessName;
            Title = response.Title;
            ReportToken = response.ReportToken;
            ContactType = response.ContactType;
            if (response.DateFirstSeen != null)
                DateFirstSeen = new Date(response.DateFirstSeen.Day, response.DateFirstSeen.Month, response.DateFirstSeen.Year);

            if (response.DateLastSeen != null)
                DateLastSeen = new Date(response.DateLastSeen.Day, response.DateLastSeen.Month, response.DateLastSeen.Year);

            CriminalRecordsFound = response.CriminalRecordsFound;
            BankruptcyRecordsFound = response.BankruptcyRecordsFound;
            UCCFilingsFound = response.UCCFilingsFound;
            LiensFound = response.LiensFound;
            JudgmentsFound = response.JudgmentsFound;

            BusinessAssociationsCount = response.BusinessAssociationsCount;
            CorporateAssociationsCount = response.CorporateAssociationsCount;
            IndividualSearchedFor = response.IndividualSearchedFor;
            if (response.Name != null)
                Name = $"{response.Name.FirstName} {response.Name.LastName}";
            if (response.Address != null)
                Address = new Address(response.Address);
            if (response.DateOfBirth != null)
            {
                if (response.DateOfBirth.DateOfBirth != null)
                    DateOfBirth = new Date(response.DateOfBirth.DateOfBirth.Day, response.DateOfBirth.DateOfBirth.Month, response.DateOfBirth.DateOfBirth.Year);
            }
        }

        public string Name { get; set; }

        public IAddress Address { get; set; }

        public IDate DateOfBirth { get; set; }
        public string BusinessName { get; set; }
        public string Title { get; set; }
        public string ReportToken { get; set; }

        public string ContactType { get; set; }

        public IDate DateFirstSeen { get; set; }

        public IDate DateLastSeen { get; set; }

        public string CriminalRecordsFound { get; set; }

        public string BankruptcyRecordsFound { get; set; }

        public string UCCFilingsFound { get; set; }

        public string LiensFound { get; set; }
        public string JudgmentsFound { get; set; }

        public string BusinessAssociationsCount { get; set; }

        public string CorporateAssociationsCount { get; set; }

        public string IndividualSearchedFor { get; set; }
    }
}