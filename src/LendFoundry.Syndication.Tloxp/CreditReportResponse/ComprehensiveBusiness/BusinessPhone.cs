﻿namespace LendFoundry.Syndication.Tloxp.CreditReportResponse.ComprehensiveBusiness
{
    public class BusinessPhone : IBusinessPhone
    {
        public BusinessPhone(string phone)
        {
            Phone = phone;
        }

        public string Phone { get; set; }
    }
}