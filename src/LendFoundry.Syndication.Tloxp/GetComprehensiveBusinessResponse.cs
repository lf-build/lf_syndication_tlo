﻿using LendFoundry.Syndication.Tloxp.CreditReportResponse.ComprehensiveBusiness;
using LendFoundry.Syndication.Tloxp.Proxy;
using System.Collections.Generic;

namespace LendFoundry.Syndication.Tloxp
{
    public class GetComprehensiveBusinessResponse : IGetComprehensiveBusinessResponse
    {
        public GetComprehensiveBusinessResponse(TLOComprehensiveBusinessSearchOutput response, Proxy.Date incorporationdate)
        {
            if (response?.BusinessRecord != null)
            {
                BusinessRecord = new ComprehensiveBusinessRecord(response.BusinessRecord);
            }
            if (incorporationdate != null)
            {
                IncorporationDate = new CreditReportResponse.ComprehensiveBusiness.Date(incorporationdate.Day, incorporationdate.Month, incorporationdate.Year);
            }
            if (response?.LienFilings != null)
            {
                LiensFiling = new List<ITloLienFiling>();
                foreach (var lien in response.LienFilings)
                {
                    LiensFiling.Add(new TloLienFiling(lien));
                }
            }
            if (response?.JudgmentFilings != null)
            {
                JugdmentFiling = new List<ITloJudgmentFiling>();
                foreach (var judgment in response.JudgmentFilings)
                {
                    JugdmentFiling.Add(new TloJudgmentFiling(judgment));
                }
            }
            if (response?.UCCFilings != null)
            {
                UccFiling = new List<ITloUCCFiling>();
                foreach (var ucc in response.UCCFilings)
                {
                    UccFiling.Add(new TloUCCFiling(ucc));
                }
            }
            if (response?.BankruptcyFilings != null)
            {
                TloBusinessBankruptcy = new List<ITloBusinessBankruptcy>();
                foreach (var bankruptcy in response.BankruptcyFilings)
                {
                    TloBusinessBankruptcy.Add(new TloBusinessBankruptcy(bankruptcy));
                }
            }
            if (response?.PropertyForeclosures != null)
            {
                ProperyForeclosure = new List<ITloPropertyForeclosure>();
                foreach (var foreclosure in response.PropertyForeclosures)
                {
                    ProperyForeclosure.Add(new TloPropertyForeclosure(foreclosure));
                }
            }
            if (response?.CorporateFilings != null)
            {
                CoorporateFiling = new List<ITloBusinessCorporateFiling>();
                foreach (var filing in response.CorporateFilings)
                {
                    CoorporateFiling.Add(new TloBusinessCorporateFiling(filing));
                }
            }
        }
        public IComprehensiveBusinessRecord BusinessRecord { get; set; }
        public IDate IncorporationDate { get; set; }
        public List<ITloLienFiling> LiensFiling { get; set; }
        public List<ITloJudgmentFiling> JugdmentFiling { get; set; }
        public List<ITloUCCFiling> UccFiling { get; set; }
        public List<ITloBusinessBankruptcy> TloBusinessBankruptcy { get; set; }
        public List<ITloPropertyForeclosure> ProperyForeclosure { get; set; }
        public List<ITloBusinessCorporateFiling> CoorporateFiling { get; set; }
    }
}