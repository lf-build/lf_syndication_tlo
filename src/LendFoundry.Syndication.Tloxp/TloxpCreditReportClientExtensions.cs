﻿using LendFoundry.Foundation.Client;
using LendFoundry.Security.Tokens;
using System;
#if DOTNET2
using Microsoft.Extensions.DependencyInjection;
#else
using Microsoft.Framework.DependencyInjection;
#endif

namespace LendFoundry.Syndication.Tloxp
{
    public static class TloxpCreditReportClientExtensions
    {
/*         public static IServiceCollection AddTloxpCreditReportService(this IServiceCollection services, string endpoint, int port)
        {
            services.AddTransient<ITloxpCreditReportClientFactory>(p => new TloxpCreditReportClientFactory(p, endpoint, port));
            services.AddTransient(p => p.GetService<ITloxpCreditReportClientFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        } */
        public static IServiceCollection AddTloxpCreditReportService(this IServiceCollection services, Uri uri)
        {
            services.AddSingleton<ITloxpCreditReportClientFactory>(p => new TloxpCreditReportClientFactory(p, uri));
            services.AddSingleton(p => p.GetService<ITloxpCreditReportClientFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }

        public static IServiceCollection AddTloxpCreditReportService(this IServiceCollection services)
        {
            services.AddSingleton<ITloxpCreditReportClientFactory>(p => new TloxpCreditReportClientFactory(p));
            services.AddSingleton(p => p.GetService<ITloxpCreditReportClientFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }
    }
}