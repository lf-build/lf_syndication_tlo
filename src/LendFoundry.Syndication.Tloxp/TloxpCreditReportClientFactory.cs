﻿using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Client;
using LendFoundry.Security.Tokens;
using System;
#if DOTNET2
using Microsoft.Extensions.DependencyInjection;
#else
using Microsoft.Framework.DependencyInjection;
#endif

namespace LendFoundry.Syndication.Tloxp
{
    internal class TloxpCreditReportClientFactory : ITloxpCreditReportClientFactory
    {
        [Obsolete("Need to use the overloaded with Uri")]
        public TloxpCreditReportClientFactory(IServiceProvider provider, string endpoint, int port)
        {
            Provider = provider;
            Endpoint = endpoint;
            Port = port;
            Uri = new UriBuilder("http", endpoint, port).Uri;
        }
        public TloxpCreditReportClientFactory(IServiceProvider provider, Uri uri = null)
        {
            Provider = provider;
            Uri = uri;
        }


        private IServiceProvider Provider { get; }

        private string Endpoint { get; }

        private int Port { get; }
        private Uri Uri { get; }

        public ITloxpService Create(ITokenReader reader)
        {
            var uri = Uri;
            if (uri == null)
            {
                var logger = Provider.GetService<ILoggerFactory>().Create(NullLogContext.Instance);
                uri = Provider.GetRequiredService<IDependencyServiceUriResolverFactory>().Create(reader, logger).Get("tloxp");
            }
            var client = Provider.GetServiceClient(reader, uri);
            return new TloxpCreditReportServiceClient(client);
        }
    }
}