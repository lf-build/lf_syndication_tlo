﻿using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Lookup;
using LendFoundry.Foundation.Services;
using LendFoundry.Syndication.Tloxp.Events;
using LendFoundry.Syndication.Tloxp.Proxy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
#if DOTNET2
using LendFoundry.EventHub;
#else
using LendFoundry.EventHub.Client;
#endif

namespace LendFoundry.Syndication.Tloxp
{
  public class TloxpService : ITloxpService
  {
    public TloxpService(ITloxpProxy TloxpClient, ITloxpConfiguration configuration,
        IComprehensivePersonReportRepository repositoryPerson,
        IComprehensiveBusinessReportRepository repositoryBusiness,
        ITenantTime tenantTime,
        IEventHubClient eventHubClient,
        ILogger logger,
        ILookupService lookup,
        ITloxpConfiguration configurationServiceTlo)
    {
      if (repositoryPerson == null)
        throw new ArgumentException($"{nameof(repositoryPerson)} is mandatory");

      if (repositoryBusiness == null)
        throw new ArgumentException($"{nameof(repositoryBusiness)} is mandatory");

      if (TloxpClient == null)
        throw new ArgumentNullException(nameof(TloxpClient));
      if (configuration == null)
        throw new ArgumentNullException(nameof(configuration));
      ProxyClient = TloxpClient;
      Configuration = configuration;
      if (tenantTime == null)
        throw new ArgumentException($"{nameof(tenantTime)} is mandatory");

      if (eventHubClient == null)
        throw new ArgumentException($"{nameof(eventHubClient)} is mandatory");

      if (logger == null)
        throw new ArgumentException($"{nameof(logger)} is mandatory");

      if (configurationServiceTlo == null)
        throw new ArgumentException($"{nameof(configurationServiceTlo)} is mandatory");
      RepositoryPerson = repositoryPerson;
      RepositoryBusiness = repositoryBusiness;
      TenantTime = tenantTime;
      EventHub = eventHubClient;
      Logger = logger;
      Lookup = lookup;
      Configuration = configurationServiceTlo;
    }

    private ITloxpConfiguration Configuration { get; }

    private ITloxpProxy ProxyClient { get; }
    private ITenantTime TenantTime { get; }
    private IEventHubClient EventHub { get; }
    private ILogger Logger { get; }
    private ILookupService Lookup { get; }
    private IComprehensivePersonReportRepository RepositoryPerson { get; }
    private IComprehensiveBusinessReportRepository RepositoryBusiness { get; }
    public async Task<IComprehensivePersonReport> CreateComprehensivePersonReport(string entityType, string entityId, IGetComprehensivePersonRequest creditReportRequest)
    {
      if (creditReportRequest == null)
        throw new ArgumentNullException(nameof(creditReportRequest));
      if (string.IsNullOrWhiteSpace(creditReportRequest.SSN))
        throw new ArgumentNullException(nameof(creditReportRequest.SSN));

      if (string.IsNullOrWhiteSpace(creditReportRequest.FirstName))
        throw new ArgumentNullException(nameof(creditReportRequest.FirstName));
      var now = TenantTime.Now;
      if (Configuration.ExpirationInDays > 0)
      {
        var creditReportFromCache = await RepositoryPerson.Get(entityType, entityId,
                creditReportRequest.SSN, creditReportRequest.FirstName);
        if (creditReportFromCache != null)
        {
          EventHub.Publish(new ComprehensivePersonReportAdded()
          {
            EntityId = entityId,
            EntityType = entityType,
            Response = creditReportFromCache.Report,
            Request = new { Ssn = creditReportRequest.SSN, FirstName = creditReportRequest.FirstName },
            ReferenceNumber = Guid.NewGuid().ToString("N")
          }).Wait();
          Logger.Info($"Comprehensive Person report retrieved from cache");
          return creditReportFromCache;
        }
      }

      IComprehensivePersonReport creditReport = null;
      try{
      var report = await ProxyClient.GetComprehensivePersonResponse(GetComprehensivePersonRequest(creditReportRequest));

      if (report == null)
        throw new NotFoundException("Comprehensive Person Report not found.");

       creditReport = new ComprehensivePersonReport
      {
        EntityId = entityId,
        EntityType = entityType,
        ExpirationDate = now.AddDays(Configuration.ExpirationInDays),
        Report = report,
        ReportDate = now,
        SSN = creditReportRequest.SSN,
        FirstName = creditReportRequest.FirstName,
        LastName = creditReportRequest.LastName,
      };
      if (Configuration.ExpirationInDays > 0)
      {
        RepositoryPerson.Add(creditReport);
        Logger.Info(
            $"Comprehensive Person Credit Report with ReportId:#{creditReport.Id}, ExpirationDate:#{creditReport.ExpirationDate}, added in database");
      }
      EventHub.Publish(new ComprehensivePersonReportAdded()
      {
        EntityId = entityId,
        EntityType = entityType,
        Response = report,
        Request = new { Ssn = creditReportRequest.SSN, FirstName = creditReportRequest.FirstName },
        ReferenceNumber = Guid.NewGuid().ToString("N")
      }).Wait();
      Logger.Info(
          $"Comprehensive Person Report with event #{nameof(ComprehensivePersonReportAdded)} was processed");
      }
      catch(Exception ex)
      {
             EventHub.Publish( "ComprehensivePersonReportFailed",new 
      {
        EntityId = entityId,
        EntityType = entityType,
        Response = ex.Message,
        Request = new { Ssn = creditReportRequest.SSN, FirstName = creditReportRequest.FirstName },
        ReferenceNumber = Guid.NewGuid().ToString("N")
      }).Wait();
      }
      return creditReport;
    }

    private TLOComprehensivePersonSearchInput GetComprehensivePersonRequest(IGetComprehensivePersonRequest request)
    {
      var tloComprehensivePersonSearchInput = new TLOComprehensivePersonSearchInput
      {
        SSN = request.SSN,
        Phone = request.Phone,
        FullName = $"{request.FirstName} {request.MiddleName} {request.LastName} {request.NameSuffix}",
        Username = Configuration.Username,
        Password = Configuration.Password,
        GLBPurpose = Configuration.GlbPurpose,
        DPPAPurpose = Configuration.DppaPurpose,
        PermissibleUseCode = Configuration.PermissibleUseCode,
        Version = Configuration.Version,
        ShowPropertyForeclosures = Configuration.PersonOutputOptions?.ShowPropertyForeclosures,
        ShowUCCFilings = Configuration.PersonOutputOptions?.ShowUccFilings,
        ShowJudgments = Configuration.PersonOutputOptions?.ShowJudgments,
        ShowLiens = Configuration.PersonOutputOptions?.ShowLiens,
        ShowBankruptcies = Configuration.PersonOutputOptions?.ShowBankruptcies,
        ShowCriminalRecords = Configuration.PersonOutputOptions?.ShowCriminalRecords,
        ExportType = String.IsNullOrEmpty(Configuration.ReportType) ? ReportType.PDF.ToString() : Configuration.ReportType
      };
      if (request.Address != null)
        tloComprehensivePersonSearchInput.Address = new Address(request.Address.Line1, request.Address.Line2, request.Address.Line3, request.Address.City, request.Address.State, request.Address.Zip, request.Address.County);
      if (request.DateOfBirth != null)
        tloComprehensivePersonSearchInput.DateOfBirth = new Proxy.Date(request.DateOfBirth.Value.Day.ToString(), request.DateOfBirth.Value.Month.ToString(), request.DateOfBirth.Value.Year.ToString());
      return tloComprehensivePersonSearchInput;
    }

    public async Task<IComprehensiveBusinessReport> CreateComprehensiveBusinessReport(string entityType, string entityId, IGetBusinessReportRequest creditReportRequest)
    {
      try
      {
        if (creditReportRequest == null)
          throw new InvalidArgumentException($"The {nameof(creditReportRequest)} cannot be null",
              nameof(creditReportRequest));

        if (string.IsNullOrWhiteSpace(creditReportRequest.FEINumber))
          throw new InvalidArgumentException($"#{nameof(creditReportRequest.FEINumber)} cannot be null",
              nameof(creditReportRequest.FEINumber));

        if (string.IsNullOrWhiteSpace(creditReportRequest.BusinessName))
          throw new InvalidArgumentException($"#{nameof(creditReportRequest.BusinessName)} cannot be null",
              nameof(creditReportRequest.BusinessName));

        if (string.IsNullOrWhiteSpace(creditReportRequest.Address.City))
          throw new InvalidArgumentException($"#{nameof(creditReportRequest.Address.City)} cannot be null",
         nameof(creditReportRequest.Address.City));

        if (string.IsNullOrWhiteSpace(creditReportRequest.Address.Zip))
          throw new InvalidArgumentException($"#{nameof(creditReportRequest.Address.Zip)} cannot be null",
         nameof(creditReportRequest.Address.Zip));

        if (string.IsNullOrWhiteSpace(creditReportRequest.Address.State))
          throw new InvalidArgumentException($"#{nameof(creditReportRequest.Address.State)} cannot be null",
         nameof(creditReportRequest.Address.State));

        entityType = ValidateEntity(entityType, entityId);
        if (Configuration == null)
          throw new ArgumentException("The configuration can't be found");
        var now = TenantTime.Now;
        if (Configuration.ExpirationInDays > 0)
        {
          var creditReportFromCache = await RepositoryBusiness.Get(entityType, entityId,
                  creditReportRequest.FEINumber);
          if (creditReportFromCache != null)
          {
            EventHub.Publish(new ComprehensiveBusinessReportAdded()
            {
              EntityId = entityId,
              EntityType = entityType,
              Response = creditReportFromCache.Report,
              Request = new { FeinNo = creditReportRequest.FEINumber },
              ReferenceNumber = Guid.NewGuid().ToString("N")
            }).Wait();
            Logger.Info($"Comprehensive Business report retrieved from cache");
            return creditReportFromCache;
          }
        }
        var response = await ComprehensiveBusinessReport(creditReportRequest);
        if (response == null)
          throw new NotFoundException("Comprehensive Business Report not found.");

        var creditReport = new ComprehensiveBusinessReport
        {
          EntityId = entityId,
          EntityType = entityType,
          ExpirationDate = now.AddDays(Configuration.ExpirationInDays),
          Report = response,
          ReportDate = now,
          FeinNumber = creditReportRequest.FEINumber,
          DunsNumber = creditReportRequest.DunsNumber
        };
        if (Configuration.ExpirationInDays > 0)
        {
          RepositoryBusiness.Add(creditReport);
          Logger.Info(
              $"Comprehensive Business Report with ReportId:#{creditReport.Id}, ExpirationDate:#{creditReport.ExpirationDate}, added in database");
        }
        EventHub.Publish(new ComprehensiveBusinessReportAdded()
        {
          EntityId = entityId,
          EntityType = entityType,
          Response = response,
          Request = new { FeinNo = creditReportRequest.FEINumber },
          ReferenceNumber = Guid.NewGuid().ToString("N")
        }).Wait();
        Logger.Info(
            $"Comprehensive Business Report with event #{nameof(ComprehensiveBusinessReportAdded)} was processed");
        return creditReport;
      }
      catch (Exception ex)
      {
        Logger.Error($"The method CreateComprehensiveBusinessReport raised an error:{ex.Message}, Full Error:", ex);
        throw;
      }
    }

    public async Task<IComprehensiveBusinessReport> GetComprehensiveBusinessReport(string entityType, string entityId, string feinno, string reportId)
    {
      try
      {
        entityType = ValidateEntity(entityType, entityId);

        if (string.IsNullOrWhiteSpace(feinno))
          throw new InvalidArgumentException($"#{nameof(feinno)} cannot be null", nameof(feinno));

        if (string.IsNullOrWhiteSpace(reportId))
          throw new InvalidArgumentException($"#{nameof(reportId)} cannot be null", nameof(reportId));

        var creditReport = await RepositoryBusiness.GetReport(entityType, entityId, feinno, reportId);
        if (creditReport == null)
          throw new NotFoundException($"The Comprehensive Business Credit Report was not found with feinno:#{feinno}, ReportId:#{reportId}");

        return creditReport;
      }
      catch (Exception ex)
      {
        Logger.Error($"The method GetComprehensiveBusinessReport raised an error:{ex.Message}, Full Error:", ex);
        throw;
      }
    }

    public async Task<IEnumerable<IComprehensiveBusinessReport>> GetComprehensiveBusinessReports(string entityType, string entityId, string feinno, int? skip = default(int?), int? take = default(int?))
    {
      try
      {
        entityType = ValidateEntity(entityType, entityId);

        if (string.IsNullOrWhiteSpace(feinno))
          throw new InvalidArgumentException($"#{nameof(feinno)} cannot be null", nameof(feinno));

        Expression<Func<IComprehensiveBusinessReport, bool>> query = q =>
            q.FeinNumber == feinno &&
            q.ExpirationDate >= TenantTime.Now;

        var totalRecords = RepositoryBusiness.Count(query);
        var creditReports = await RepositoryBusiness.All(query, skip, take);

        if (!creditReports.Any())
          throw new NotFoundException($"Comprehensive Business Credit Report: #{feinno} has no credit reports");

        if ((totalRecords > creditReports.Count()) && (skip.HasValue && skip.Value >= totalRecords))
          throw new NotFoundException($"Comprehensive Business Credit Report: #{feinno} has only {totalRecords} credit reports");

        Logger.Info($"Comprehensive Business CreditReport list searched the requested. #{creditReports.Count()} results");

        return creditReports;
      }
      catch (Exception ex)
      {
        Logger.Error($"The method GetComprehensiveBusinessReports raised an error:{ex.Message}, Full Error:", ex);
        throw;
      }
    }

    public async Task<IComprehensivePersonReport> GetComprehensivePersonReport(string entityType, string entityId, string ssn, string reportId)
    {
      try
      {
        entityType = ValidateEntity(entityType, entityId);

        if (string.IsNullOrWhiteSpace(ssn))
          throw new InvalidArgumentException($"#{nameof(ssn)} cannot be null", nameof(ssn));

        if (string.IsNullOrWhiteSpace(reportId))
          throw new InvalidArgumentException($"#{nameof(reportId)} cannot be null", nameof(reportId));

        var creditReport = await RepositoryPerson.GetReport(entityType, entityId, ssn, reportId);
        if (creditReport == null)
          throw new NotFoundException($"The Comprehensive Person Credit Report was not found with ssn:#{ssn}, ReportId:#{reportId}");

        return creditReport;
      }
      catch (Exception ex)
      {
        Logger.Error($"The methodGetComprehensivePersonReport raised an error:{ex.Message}, Full Error:", ex);
        throw;
      }
    }

    public async Task<IEnumerable<IComprehensivePersonReport>> GetComprehensivePersonReports(string entityType, string entityId, string ssn, int? skip = default(int?), int? take = default(int?))
    {
      try
      {
        entityType = ValidateEntity(entityType, entityId);

        if (string.IsNullOrWhiteSpace(ssn))
          throw new InvalidArgumentException($"#{nameof(ssn)} cannot be null", nameof(ssn));

        Expression<Func<IComprehensivePersonReport, bool>> query = q =>
            q.SSN == ssn &&
            q.ExpirationDate >= TenantTime.Now;

        var totalRecords = RepositoryPerson.Count(query);
        var creditReports = await RepositoryPerson.All(query, skip, take);

        if (!creditReports.Any())
          throw new NotFoundException($"Comprehensive person Credit Report: #{ssn} has no credit reports");

        if ((totalRecords > creditReports.Count()) && (skip.HasValue && skip.Value >= totalRecords))
          throw new NotFoundException($"Comprehensive Person Credit Report: #{ssn} has only {totalRecords} credit reports");

        Logger.Info($"Comprehensive Person CreditReport list searched the requested. #{creditReports.Count()} results");

        return creditReports;
      }
      catch (Exception ex)
      {
        Logger.Error($"The method GetComprehensivePersonReports raised an error:{ex.Message}, Full Error:", ex);
        throw;
      }
    }


    private async Task<TLOComprehensiveBusinessSearchOutput> ComprehensiveBusinessReport(IGetBusinessReportRequest request)
    {
      if (request == null)
        throw new ArgumentNullException(nameof(request));

      if (string.IsNullOrWhiteSpace(request.FEINumber))
        throw new ArgumentNullException(nameof(request.FEINumber));

      var tloBusinessSearchOutput = await ProxyClient.GetBusinessResponse(GetBusinessRequest(request));

      if (tloBusinessSearchOutput.BusinessSearchOutputRecords.Any())
      {
        var businessSearchOutputRecord = tloBusinessSearchOutput.BusinessSearchOutputRecords.FirstOrDefault();
        var incorporationdate = businessSearchOutputRecord.IncorporationDate;
        if (!string.IsNullOrEmpty(businessSearchOutputRecord?.BusinessToken))
        {
          GetComprehensiveBusinessRequest comprehensiveBusinessRequest = new GetComprehensiveBusinessRequest
          {
            BusinessToken = businessSearchOutputRecord.BusinessToken,
          };
          return await ProxyClient.GetComprehensiveBusinessResponse(GetTransUnionComprehensiveBusinessRequest(comprehensiveBusinessRequest));
        }
      }
      return null;
    }

    public async Task<IComprehensiveBusinessReport> ComprehensiveBusinessReportBasedOnToken(string entityType, string entityId, IGetComprehensiveBusinessRequest request)
    {
      if (request == null)
        throw new ArgumentNullException(nameof(request));

      if (string.IsNullOrWhiteSpace(request.BusinessToken))
        throw new ArgumentNullException(nameof(request.BusinessToken));
      GetComprehensiveBusinessRequest comprehensiveBusinessRequest = new GetComprehensiveBusinessRequest
      {
        BusinessToken = request.BusinessToken,
      };
      var now = TenantTime.Now;
      var response = await ProxyClient.GetComprehensiveBusinessResponse(GetTransUnionComprehensiveBusinessRequest(comprehensiveBusinessRequest));
      var creditReport = new ComprehensiveBusinessReport
      {
        EntityId = entityId,
        EntityType = entityType,
        ExpirationDate = now.AddDays(Configuration.ExpirationInDays),
        Report = response,
        ReportDate = now,
        FeinNumber = request.FEINumber
      };
      if (Configuration.ExpirationInDays > 0)
      {
        RepositoryBusiness.Add(creditReport);
        Logger.Info(
            $"Comprehensive Business Report with ReportId:#{creditReport.Id}, ExpirationDate:#{creditReport.ExpirationDate}, added in database");
      }
      EventHub.Publish(new ComprehensiveBusinessReportAdded()
      {
        EntityId = entityId,
        EntityType = entityType,
        Response = response,
        Request = new { FeinNo = request.FEINumber },
        ReferenceNumber = Guid.NewGuid().ToString("N")
      }).Wait();
      Logger.Info(
          $"Comprehensive Business Report with event #{nameof(ComprehensiveBusinessReportAdded)} was processed");
      return creditReport;
    }
    public async Task<IComprehensiveBusinessSearch> ComprehensiveBusinessSearch(string entityId, string entityType, IGetBusinessReportRequest request)
    {
      try
      {
        if (request == null)
          throw new InvalidArgumentException($"The {nameof(request)} cannot be null",
              nameof(request));

        if (string.IsNullOrWhiteSpace(request.FEINumber))
          throw new InvalidArgumentException($"#{nameof(request.FEINumber)} cannot be null",
              nameof(request.FEINumber));

        if (string.IsNullOrWhiteSpace(request.BusinessName))
          throw new InvalidArgumentException($"#{nameof(request.BusinessName)} cannot be null",
              nameof(request.BusinessName));

        if (string.IsNullOrWhiteSpace(request.Address.City))
          throw new InvalidArgumentException($"#{nameof(request.Address.City)} cannot be null",
         nameof(request.Address.City));

        if (string.IsNullOrWhiteSpace(request.Address.Zip))
          throw new InvalidArgumentException($"#{nameof(request.Address.Zip)} cannot be null",
         nameof(request.Address.Zip));

        if (string.IsNullOrWhiteSpace(request.Address.State))
          throw new InvalidArgumentException($"#{nameof(request.Address.State)} cannot be null",
         nameof(request.Address.State));

        List<ComprehensiveBusinessSearch> businessSearchList = new List<ComprehensiveBusinessSearch>();
        var tloBusinessSearchOutput = await ProxyClient.GetBusinessResponse(GetBusinessRequest(request));

        if (tloBusinessSearchOutput.BusinessSearchOutputRecords.Any())
        {
          ComprehensiveBusinessSearch comprehensiveBusinessSearch = new ComprehensiveBusinessSearch()
          {
            BusinessSearchOutputRecords = tloBusinessSearchOutput
          };
          return comprehensiveBusinessSearch;
        }

        return null;
      }
      catch (Exception ex)
      {
        Logger.Error($"The methodGetComprehensivePersonReport raised an error:{ex.Message}, Full Error:", ex);
        throw;
      }
    }

    private TLOGenericSearchInput GetBusinessRequest(IGetBusinessReportRequest request)
    {
      if (string.IsNullOrWhiteSpace(Configuration.Username))
        throw new ArgumentNullException(nameof(Configuration.Username));
      if (string.IsNullOrWhiteSpace(Configuration.Password))
        throw new ArgumentNullException(nameof(Configuration.Password));
      var tloGenericSearchInput = new TLOGenericSearchInput
      {
        BusinessName = request.BusinessName,
        FEINumber = request.FEINumber,

        DunsNumber = request.DunsNumber,
        Username = Configuration.Username,
        Password = Configuration.Password,
        GLBPurpose = Configuration.GlbPurpose,
        DPPAPurpose = Configuration.DppaPurpose,
        PermissibleUseCode = Configuration.PermissibleUseCode,
        Version = Configuration.Version
      };
      if (request.Address != null)
        tloGenericSearchInput.Address = new Address(request.Address.Line1, request.Address.Line2, request.Address.Line3, request.Address.City, request.Address.State, request.Address.Zip, request.Address.County);
      return tloGenericSearchInput;
    }

    private TLOComprehensiveBusinessSearchInput GetTransUnionComprehensiveBusinessRequest(IGetComprehensiveBusinessRequest request)
    {
      var tloComprehensiveBusinessSearchInput = new TLOComprehensiveBusinessSearchInput
      {
        BusinessToken = request.BusinessToken,
        Username = Configuration.Username,
        Password = Configuration.Password,
        GLBPurpose = Configuration.GlbPurpose,
        DPPAPurpose = Configuration.DppaPurpose,
        PermissibleUseCode = Configuration.PermissibleUseCode,
        Version = Configuration.Version,
        ShowPropertyForeclosures = Configuration.BusinessOutputOptions?.ShowPropertyForeclosures,
        ShowBankruptcies = Configuration.BusinessOutputOptions?.ShowBankruptcies,
        ShowJudgements = Configuration.BusinessOutputOptions?.ShowJudgements,
        ShowLiens = Configuration.BusinessOutputOptions?.ShowLiens,
        ShowUCCFilings = Configuration.BusinessOutputOptions?.ShowUCCFilings,
      };
      return tloComprehensiveBusinessSearchInput;
    }


    private string ValidateEntity(string entityType, string entityId)
    {
      entityType = EnsureEntityType(entityType);
      if (string.IsNullOrWhiteSpace(entityId))
        throw new InvalidArgumentException($"The field #{nameof(entityId)} cannot be null", nameof(entityId));
      return entityType;
    }

    public string EnsureEntityType(string entityType)
    {
      if (string.IsNullOrWhiteSpace(entityType))
        throw new InvalidArgumentException("Invalid Entity Type");
      entityType = entityType.ToLower();
      var validEntityTypes = Lookup.GetLookupEntry("entityTypes", entityType);
      if (validEntityTypes == null || !validEntityTypes.Any())
        throw new InvalidArgumentException("Invalid Entity Type");
      return entityType;
    }
  }
}