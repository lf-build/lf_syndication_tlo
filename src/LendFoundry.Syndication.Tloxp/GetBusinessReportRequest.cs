﻿using LendFoundry.Syndication.Tloxp.CreditReportRequest.AddressBusinessPerson;
namespace LendFoundry.Syndication.Tloxp
{
  public class GetBusinessReportRequest : IGetBusinessReportRequest
  {
    public string FEINumber { get; set; }

    public string DunsNumber { get; set; }

    public string BusinessName { get; set; }

    public AddressRequest Address { get; set; }
  }
}