﻿using LendFoundry.Foundation.Client;
using System.Collections.Generic;

namespace LendFoundry.Syndication.Tloxp
{
    public class TloxpConfiguration : ITloxpConfiguration, IDependencyConfiguration
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public string GlbPurpose { get; set; }
        public string DppaPurpose { get; set; }
        public string PermissibleUseCode { get; set; }
        public string Version { get; set; }
        public string Url { get; set; }
        public IPersonOutputOptions PersonOutputOptions { get; set; }
        public IBusinessOutputOptions BusinessOutputOptions { get; set; }
        public int ExpirationInDays { get; set; }
        public string ReportType { get; set; }
        public Dictionary<string, string> Dependencies { get; set; }
        public string Database { get; set; }
        public string ConnectionString { get; set; }

    }
}