﻿namespace LendFoundry.Syndication.Tloxp
{
  public class GetComprehensiveBusinessRequest : IGetComprehensiveBusinessRequest
  {
    public string BusinessToken { get; set; }

    public string FEINumber { get; set; }
  }
}