﻿using System;
using System.Collections.Generic;

namespace LendFoundry.Syndication.Tloxp.Proxy
{
    public class TloxpException : Exception
    {
        public Dictionary<string, string> Errors { get; set; }
    }
}