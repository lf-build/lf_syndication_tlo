﻿using System;
using System.Net;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.Tloxp.Proxy
{
  public class TloxpProxy : ITloxpProxy
  {
    public TloxpProxy(ITloxpConfiguration configuration)
    {
      ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11;
      if (configuration == null)
        throw new ArgumentNullException(nameof(configuration));
      Configuration = configuration;
    }

    private ITloxpConfiguration Configuration { get; }

    public async Task<TLOComprehensiveBusinessSearchOutput> GetComprehensiveBusinessResponse(TLOComprehensiveBusinessSearchInput request)
    {
      return await Task.Run(() =>
      {
        try
        {
          var serviceClient = GetServiceClient();
          var result = serviceClient.ComprehensiveBusinessSearchAsync(request).Result;

          if (!string.IsNullOrEmpty(result.ErrorMessage))
          {
            var exception = SetErrors(result.ErrorCode.ToString(), result.ErrorMessage);
            throw exception;
          }
          return result;
        }
        catch (TloxpException)
        {
          throw;
        }
        catch (Exception)
        {
          throw;
        }
      });
    }

    public async Task<TLOBusinessSearchOutput> GetBusinessResponse(TLOGenericSearchInput request)
    {
      return await Task.Run(() =>
      {
        try
        {
          var serviceClient = GetServiceClient();
          var result = serviceClient.BusinessSearchAsync(request).Result;
          if (!string.IsNullOrEmpty(result.ErrorMessage))
          {
            var exception = SetErrors(result.ErrorCode.ToString(), result.ErrorMessage);

            throw exception;
          }
          return result;
        }
        catch (TloxpException)
        {
          throw;
        }
        catch (Exception)
        {
          throw;
        }
      });
    }

    public async Task<TLOComprehensivePersonSearchOutput> GetComprehensivePersonResponse(TLOComprehensivePersonSearchInput request1)
    {
      return await Task.Run(() =>
      {
        try
        {
          var serviceClient = GetServiceClient();
          var result = serviceClient.ComprehensivePersonSearchAsync(request1).Result;
          if (result != null)
          {
            if (!string.IsNullOrEmpty(result.ErrorMessage))
            {
              var exception = SetErrors(result.ErrorCode.ToString(), result.ErrorMessage);
              throw exception;
            }
          }
          return result;
        }
        catch (TloxpException)
        {
          throw;
        }
        catch (Exception)
        {
          throw;
        }
      }
          );
    }

    public async Task<TLOPersonSearchOutput> GetPersonResponse(TLOGenericSearchInput request)
    {
      return await Task.Run(() =>
      {
        try
        {
          var serviceClient = GetServiceClient();
          var result = serviceClient.PersonSearchAsync(request).Result;
          if (!string.IsNullOrEmpty(result.ErrorMessage))
          {
            var exception = SetErrors(result.ErrorCode.ToString(), result.ErrorMessage);

            throw exception;
          }
          return result;
        }
        catch (TloxpException)
        {
          throw;
        }
        catch (Exception)
        {
          throw;
        }
      });
    }

    private TloxpException SetErrors(string errorCode, string errorMsg)
    {
      return new TloxpException()
      {
        Errors = new Dictionary<string, string> {
                    {
                       errorCode,errorMsg
                    }
                }
      };
    }

    private ITLOWebServiceSoap GetServiceClient()
    {
      TLOWebServiceSoapClient serviceClient = new TLOWebServiceSoapClient(TLOWebServiceSoapClient.EndpointConfiguration.TLOWebServiceSoap, Configuration.Url);
      return serviceClient;
    }
  }
}