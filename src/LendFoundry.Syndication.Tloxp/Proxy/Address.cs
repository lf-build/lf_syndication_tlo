﻿namespace LendFoundry.Syndication.Tloxp.Proxy
{
    public partial class Address { 
        public Address()
        {
        }

        public Address(string Line1, string Line2, string Line3, string City, string State, string Zip, string country)
        {
            this.Line1 = Line1;
            this.Line2 = Line2;
            this.Line3 = Line3;
            this.City = City;
            this.State = State;
            this.Zip = Zip;
            this.County = country;
        }
    }
}