﻿namespace LendFoundry.Syndication.Tloxp.Proxy
{
    public partial class Date
    {
        public Date()
        {
        }

        public Date(string day, string month, string year)
        {
            this.Day = day;
            this.Month = month;
            this.Year = year;
        }
    }
}