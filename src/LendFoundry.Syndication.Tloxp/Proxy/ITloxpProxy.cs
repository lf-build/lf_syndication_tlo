﻿using System.Threading.Tasks;

namespace LendFoundry.Syndication.Tloxp.Proxy
{
    public interface ITloxpProxy
    {
        Task<TLOPersonSearchOutput> GetPersonResponse(TLOGenericSearchInput request);

        Task<TLOComprehensivePersonSearchOutput> GetComprehensivePersonResponse(TLOComprehensivePersonSearchInput request);

        Task<TLOBusinessSearchOutput> GetBusinessResponse(TLOGenericSearchInput request);

        Task<TLOComprehensiveBusinessSearchOutput> GetComprehensiveBusinessResponse(TLOComprehensiveBusinessSearchInput request);
    }
}