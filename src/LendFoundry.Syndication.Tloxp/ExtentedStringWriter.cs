using System.IO;
using System.Text;

namespace LendFoundry.Syndication.Tloxp
{
    public sealed class ExtentedStringWriter : StringWriter
    {
        public ExtentedStringWriter(Encoding desiredEncoding)
        {
            Encoding = desiredEncoding;
        }

        public override Encoding Encoding { get; }
    }
}