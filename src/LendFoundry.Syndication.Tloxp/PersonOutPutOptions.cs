﻿namespace LendFoundry.Syndication.Tloxp
{
    public class PersonOutputOptions : IPersonOutputOptions
    {
        public string ShowPropertyForeclosures { get; set; } = "1";
        public string ShowUccFilings { get; set; } = "1";
        public string ShowJudgments { get; set; } = "1";
        public string ShowLiens { get; set; } = "1";
        public string ShowBankruptcies { get; set; } = "1";
        public string ShowCriminalRecords { get; set; } = "1";
    }
}