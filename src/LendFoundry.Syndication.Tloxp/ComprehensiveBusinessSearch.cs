
using System;

namespace LendFoundry.Syndication.Tloxp
{
  public class ComprehensiveBusinessSearch : IComprehensiveBusinessSearch
  {
    public object BusinessSearchOutputRecords { get; set; }

  }
}