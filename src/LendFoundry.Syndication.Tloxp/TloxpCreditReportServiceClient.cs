﻿using LendFoundry.Foundation.Client;
using RestSharp;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.Tloxp
{
  internal class TloxpCreditReportServiceClient : ITloxpService
  {
    public TloxpCreditReportServiceClient(IServiceClient client)
    {
      Client = client;
    }

    private IServiceClient Client { get; }

    public async Task<IComprehensiveBusinessReport> CreateComprehensiveBusinessReport(string entityType, string entityId, IGetBusinessReportRequest creditReportRequest)
    {
      var request = new RestRequest("{entityType}/{entityId}/business", Method.POST);
      request.AddUrlSegment("entityType", entityType);
      request.AddUrlSegment("entityId", entityId);
      request.AddJsonBody(creditReportRequest);
      return await Client.ExecuteAsync<ComprehensiveBusinessReport>(request);
    }

    public async Task<IComprehensivePersonReport> CreateComprehensivePersonReport(string entityType, string entityId, IGetComprehensivePersonRequest creditReportRequest)
    {
      var request = new RestRequest("{entityType}/{entityId}/person", Method.POST);
      request.AddUrlSegment("entityType", entityType);
      request.AddUrlSegment("entityId", entityId);
      request.AddJsonBody(creditReportRequest);
      return await Client.ExecuteAsync<ComprehensivePersonReport>(request);
    }

    public async Task<IComprehensiveBusinessReport> GetComprehensiveBusinessReport(string entityType, string entityId, string feinno, string reportId)
    {
      var request = new RestRequest("/business/{entityType}/{entityId}/{feinno}/{reportId}", Method.GET);
      request.AddUrlSegment(nameof(entityType), entityType);
      request.AddUrlSegment(nameof(entityId), entityId);
      request.AddUrlSegment(nameof(feinno), feinno);
      request.AddUrlSegment(nameof(reportId), reportId);
      return await Client.ExecuteAsync<ComprehensiveBusinessReport>(request);
    }

    public async Task<IEnumerable<IComprehensiveBusinessReport>> GetComprehensiveBusinessReports(string entityType, string entityId, string feinno, int? skip = default(int?), int? take = default(int?))
    {
      var request = new RestRequest("/businessreports/{entityType}/{entityId}/{feinno}/{skip?}/{take?}", Method.GET);
      request.AddUrlSegment(nameof(entityType), entityType);
      request.AddUrlSegment(nameof(entityId), entityId);
      request.AddUrlSegment(nameof(feinno), feinno);
      if (skip != null) request.AddUrlSegment(nameof(skip), skip.Value.ToString());
      if (take != null) request.AddUrlSegment(nameof(take), take.Value.ToString());
      return await Client.ExecuteAsync<List<ComprehensiveBusinessReport>>(request);
    }

    public async Task<IComprehensivePersonReport> GetComprehensivePersonReport(string entityType, string entityId, string ssn, string reportId)
    {
      var request = new RestRequest("/person/{entityType}/{entityId}/{ssn}/{reportId}", Method.GET);
      request.AddUrlSegment(nameof(entityType), entityType);
      request.AddUrlSegment(nameof(entityId), entityId);
      request.AddUrlSegment(nameof(ssn), ssn);
      request.AddUrlSegment(nameof(reportId), reportId);
      return await Client.ExecuteAsync<ComprehensivePersonReport>(request);
    }

    public async Task<IEnumerable<IComprehensivePersonReport>> GetComprehensivePersonReports(string entityType, string entityId, string ssn, int? skip = default(int?), int? take = default(int?))
    {
      var request = new RestRequest("/personreports/{entityType}/{entityId}/{ssn}/{skip?}/{take?}", Method.GET);
      request.AddUrlSegment(nameof(entityType), entityType);
      request.AddUrlSegment(nameof(entityId), entityId);
      request.AddUrlSegment(nameof(ssn), ssn);
      if (skip != null) request.AddUrlSegment(nameof(skip), skip.Value.ToString());
      if (take != null) request.AddUrlSegment(nameof(take), take.Value.ToString());
      return await Client.ExecuteAsync<List<ComprehensivePersonReport>>(request);
    }
    public async Task<IComprehensiveBusinessReport> ComprehensiveBusinessReportBasedOnToken(string entityType, string entityId, IGetComprehensiveBusinessRequest token)
    {
      var request = new RestRequest("{entityType}/{entityId}/business/report", Method.POST);
      request.AddUrlSegment("entityType", entityType);
      request.AddUrlSegment("entityId", entityId);
      request.AddJsonBody(token);
      return await Client.ExecuteAsync<ComprehensiveBusinessReport>(request);
    }

    public async Task<IComprehensiveBusinessSearch> ComprehensiveBusinessSearch(string entityType, string entityId, IGetBusinessReportRequest businessRequest)
    {
      var request = new RestRequest("{entityType}/{entityId}/business/search", Method.POST);
      request.AddUrlSegment("entityType", entityType);
      request.AddUrlSegment("entityId", entityId);
      request.AddJsonBody(businessRequest);
      return await Client.ExecuteAsync<IComprehensiveBusinessSearch>(request);
    }
  }
}