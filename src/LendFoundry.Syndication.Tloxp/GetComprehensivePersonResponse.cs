﻿using LendFoundry.Syndication.Tloxp.CreditReportResponse.ComprehensivePerson;
using LendFoundry.Syndication.Tloxp.Proxy;
using System.Collections.Generic;

namespace LendFoundry.Syndication.Tloxp
{
    public class GetComprehensivePersonResponse : IGetComprehensivePersonResponse
    {
        public GetComprehensivePersonResponse(TLOComprehensivePersonSearchOutput response)
        {
            if (response != null)
            {
                PersonRecord = new ComprehensivePersonRecord(response);
                if (response?.Bankruptcies != null)
                {
                    TloPersonBankruptcy = new List<ITloPersonBankruptcy>();
                    foreach (var bankruptcy in response.Bankruptcies)
                    {
                        TloPersonBankruptcy.Add(new TloPersonBankruptcy(bankruptcy));
                    }
                }
                if (response?.CriminalRecordsMatch != null)
                {
                    CriminalRecords = new List<ITloPersonCriminal>();
                    foreach (var criminalrecords in response.CriminalRecordsMatch)
                    {
                        CriminalRecords.Add(new TloPersonCriminal(criminalrecords));
                    }
                }
                if (response?.PropertyForeclosures != null)
                {
                    PropertyForeclosure = new List<ITloPropertyForeclosure>();
                    foreach (var foreclosure in response.PropertyForeclosures)
                    {
                        PropertyForeclosure.Add(new TloPropertyForeclosure(foreclosure));
                    }
                }
                if (response?.Liens != null)
                {
                    Liens = new List<ITloLienFiling>();
                    foreach (var lien in response.Liens)
                    {
                        Liens.Add(new TloLienFiling(lien));
                    }
                }
                if (response?.Judgments != null)
                {
                    JudgmentFiling = new List<ITloJudgmentFiling>();
                    foreach (var judgment in response.Judgments)
                    {
                        JudgmentFiling.Add(new TloJudgmentFiling(judgment));
                    }
                }
                if (response?.UCCFilings != null)
                {
                    UccFiling = new List<ITloUccFiling>();
                    foreach (var ucc in response.UCCFilings)
                    {
                        UccFiling.Add(new TloUccFiling(ucc));
                    }
                }
            }
        }
        public IComprehensivePersonRecord PersonRecord { get; set; }
        public List<ITloLienFiling> Liens { get; set; }
        public List<ITloJudgmentFiling> JudgmentFiling { get; set; }
        public List<ITloUccFiling> UccFiling { get; set; }
        public List<ITloPersonBankruptcy> TloPersonBankruptcy { get; set; }
        public List<ITloPersonCriminal> CriminalRecords { get; set; }
        public List<ITloPropertyForeclosure> PropertyForeclosure { get; set; }
    }
}