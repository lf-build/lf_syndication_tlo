﻿using LendFoundry.Syndication.Tloxp.CreditReportResponse.ComprehensivePerson;
using System.Collections.Generic;

namespace LendFoundry.Syndication.Tloxp
{
    public interface IGetComprehensivePersonResponse
    {
        IComprehensivePersonRecord PersonRecord { get; set; }
        List<ITloLienFiling> Liens { get; set; }
        List<ITloJudgmentFiling> JudgmentFiling { get; set; }
        List<ITloUccFiling> UccFiling { get; set; }
        List<ITloPersonBankruptcy> TloPersonBankruptcy { get; set; }
        List<ITloPersonCriminal> CriminalRecords { get; set; }
        List<ITloPropertyForeclosure> PropertyForeclosure { get; set; }
    }
}