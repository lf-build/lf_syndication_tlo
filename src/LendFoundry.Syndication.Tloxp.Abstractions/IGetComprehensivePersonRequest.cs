﻿using LendFoundry.Syndication.Tloxp.CreditReportRequest.AddressBusinessPerson;
using System;

namespace LendFoundry.Syndication.Tloxp
{
  public interface IGetComprehensivePersonRequest
  {
    string NameSuffix { get; set; }
    string FirstName { get; set; }
    string MiddleName { get; set; }
    string LastName { get; set; }
    string SSN { get; set; }
    IAddressRequest Address { get; set; }
    string Phone { get; set; }
    DateTime? DateOfBirth { get; set; }
  }
}