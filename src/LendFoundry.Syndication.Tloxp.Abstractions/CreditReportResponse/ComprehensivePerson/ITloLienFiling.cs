﻿namespace LendFoundry.Syndication.Tloxp.CreditReportResponse.ComprehensivePerson
{
    public interface ITloLienFiling
    {
        string FilingType { get; set; }
        string TotalLienAmount { get; set; }
        IDate FilingDate { get; set; }
        IDate TaxLienDate { get; set; }
        string FederalTaxLienArea { get; set; }
    }
}