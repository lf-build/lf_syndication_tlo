﻿namespace LendFoundry.Syndication.Tloxp.CreditReportResponse.ComprehensivePerson
{
    public interface ITloJudgmentFiling
    {
        string FilingType { get; set; }
        IDate FilingDate { get; set; }
        string TotalJudgmentAmount { get; set; }
    }
}