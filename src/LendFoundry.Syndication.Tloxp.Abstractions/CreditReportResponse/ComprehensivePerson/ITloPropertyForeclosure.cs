﻿using System.Collections.Generic;

namespace LendFoundry.Syndication.Tloxp.CreditReportResponse.ComprehensivePerson
{
    public interface ITloPropertyForeclosure
    {
        string FIPSCounty { get; set; }
        IDate FilingDate { get; set; }
        string AmountOfDefault { get; set; }
        string FinalJudgmentAmount { get; set; }
        string CourtCaseNumber { get; set; }
        List<ITloName> Owners { get; set; }
        ITloAddressRequest Address { get; set; }
        ITloAddressRequest MailingAddress { get; set; }
        ITloAddressRequest AuctionCallAddress { get; set; }
        ITloName Lender { get; set; }
        ITloAddressRequest LenderAddress { get; set; }
        ITloAddressRequest TrusteeAddress { get; set; }
    }
}