﻿namespace LendFoundry.Syndication.Tloxp.CreditReportResponse.ComprehensivePerson
{
    public interface ITloIPAddressRecord
    {
        string IPAddress { get; set; }

        string Country { get; set; }

        string Region { get; set; }

        string City { get; set; }

        string PostalCode { get; set; }

        string ISP { get; set; }

        string BusinessName { get; set; }

        string DomainName { get; set; }

        string Latitude { get; set; }

        string Longitude { get; set; }

        IDate DateFirstSeen { get; set; }

        ITloTime TimeFirstSeen { get; set; }

        IDate DateLastSeen { get; set; }

        ITloTime TimeLastSeen { get; set; }
    }
}