﻿using System.Collections.Generic;

namespace LendFoundry.Syndication.Tloxp.CreditReportResponse.ComprehensivePerson
{
    public interface IComprehensivePersonRecord
    {
        string ReportToken { get; set; }
        List<IPersonName> Names { get; set; }

        List<ISsnRecord> SsnRecords { get; set; }

        List<IDate> DatesOfBirth { get; set; }

        List<IDate> DatesOfDeath { get; set; }

        List<IAddress> Addresses { get; set; }
    }
}