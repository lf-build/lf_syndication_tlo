﻿namespace LendFoundry.Syndication.Tloxp.CreditReportResponse.ComprehensivePerson
{
    public interface ICrimeDetails
    {
        string CaseNumber { get; set; }
        string OffenceCode { get; set; }
        IDate DispositionDate { get; set; }
        string Disposition { get; set; }
        string Statute { get; set; }
        string Plea { get; set; }
        IDate FilingDate { get; set; }
    }
}