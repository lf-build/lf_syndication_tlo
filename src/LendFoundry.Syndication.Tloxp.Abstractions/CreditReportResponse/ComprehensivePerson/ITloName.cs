﻿namespace LendFoundry.Syndication.Tloxp.CreditReportResponse.ComprehensivePerson
{
    public interface ITloName
    {
        string Observations { get; set; }

        string Score { get; set; }

        string Display { get; set; }

        string Source { get; set; }
    }
}