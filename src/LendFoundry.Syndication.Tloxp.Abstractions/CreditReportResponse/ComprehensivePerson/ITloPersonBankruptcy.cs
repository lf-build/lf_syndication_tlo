﻿namespace LendFoundry.Syndication.Tloxp.CreditReportResponse.ComprehensivePerson
{
    public interface ITloPersonBankruptcy
    {
        IDate FilingDate { get; set; }

        IDate DischargeDate { get; set; }

        string Chapter { get; set; }
    }
}