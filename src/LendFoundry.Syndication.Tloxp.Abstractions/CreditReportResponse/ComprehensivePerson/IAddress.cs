﻿namespace LendFoundry.Syndication.Tloxp.CreditReportResponse.ComprehensivePerson
{
    public interface IAddress
    {
        string Line1 { get; set; }
        string Line2 { get; set; }

        string Line3 { get; set; }
        string City { get; set; }
        string State { get; set; }

        string Zip { get; set; }
        string County { get; set; }
    }
}