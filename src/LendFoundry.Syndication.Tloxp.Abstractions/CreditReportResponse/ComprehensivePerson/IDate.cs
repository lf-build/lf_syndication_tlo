﻿namespace LendFoundry.Syndication.Tloxp.CreditReportResponse.ComprehensivePerson
{
    public interface IDate
    {
        string Day { get; set; }
        string Month { get; set; }
        string Year { get; set; }
    }
}