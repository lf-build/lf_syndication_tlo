﻿namespace LendFoundry.Syndication.Tloxp.CreditReportResponse.ComprehensivePerson
{
    public interface ISsnRecord
    {
        string Ssn { get; set; }
        string First5Masked { get; set; }
        string Last4Masked { get; set; }
    }
}