﻿namespace LendFoundry.Syndication.Tloxp.CreditReportResponse.ComprehensivePerson
{
    public interface IWarrantDetails
    {
        string OffenceType { get; set; }
        string CaseNumber { get; set; }
    }
}