﻿using System.Collections.Generic;

namespace LendFoundry.Syndication.Tloxp.CreditReportResponse.ComprehensivePerson
{
    public interface ITloPersonCriminal
    {
        List<ICrimeDetails> CrimeDetails { get; set; }
        List<IWarrantDetails> WarrantDetails { get; set; }
    }
}