﻿namespace LendFoundry.Syndication.Tloxp.CreditReportResponse.ComprehensivePerson
{
    public interface ITloTime
    {
        string Hour { get; set; }

        string Minute { get; set; }

        string Second { get; set; }
    }
}