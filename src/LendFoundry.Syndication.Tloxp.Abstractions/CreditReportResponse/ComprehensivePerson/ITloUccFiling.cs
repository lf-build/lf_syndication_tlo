﻿namespace LendFoundry.Syndication.Tloxp.CreditReportResponse.ComprehensivePerson
{
    public interface ITloUccFiling
    {
        string FilingType { get; set; }
        string FilingNumber { get; set; }
        IDate FilingDate { get; set; }
        string FilingOfficeName { get; set; }
    }
}