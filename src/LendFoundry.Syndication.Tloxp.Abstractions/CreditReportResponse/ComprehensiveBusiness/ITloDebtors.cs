﻿namespace LendFoundry.Syndication.Tloxp.CreditReportResponse.ComprehensiveBusiness
{
    public interface ITloDebtors
    {
        string BusinessName { get; set; }

        string FEINumber { get; set; }

        string Name { get; set; }

        string ReportToken { get; set; }

        string BusinessToken { get; set; }
        ITloBasicDateOfBirthRecord DateOfBirth { get; set; }
        ITloBasicSSNRecord PreferredSSNRecord { get; set; }
        IAddress DebtorAddress { get; set; }
    }
}