﻿namespace LendFoundry.Syndication.Tloxp.CreditReportResponse.ComprehensiveBusiness
{
    public interface ITloJudgmentFiling
    {
        IDate FilingDate { get; set; }
        string JudgmentType { get; set; }
        string Status { get; set; }
    }
}