﻿using System.Collections.Generic;

namespace LendFoundry.Syndication.Tloxp.CreditReportResponse.ComprehensiveBusiness
{
    public interface ITloUCCFiling
    {
        string FilingType { get; set; }
        string FilingNumber { get; set; }
        IDate FilingDate { get; set; }
        string FilingOfficeName { get; set; }

        IAddress FilingOfficeAddress { get; set; }
        List<ITloDebtors> Debtors { get; set; }
        List<ITloSecuredParties> SecurityParty { get; set; }
    }
}