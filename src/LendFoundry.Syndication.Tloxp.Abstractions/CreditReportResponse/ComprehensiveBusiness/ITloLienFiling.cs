﻿namespace LendFoundry.Syndication.Tloxp.CreditReportResponse.ComprehensiveBusiness
{
    public interface ITloLienFiling
    {
        IDate FilingDate { get; set; }
        string LienAmount { get; set; }
        string LienType { get; set; }
    }
}