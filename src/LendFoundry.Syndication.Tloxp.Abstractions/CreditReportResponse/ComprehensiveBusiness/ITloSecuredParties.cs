﻿namespace LendFoundry.Syndication.Tloxp.CreditReportResponse.ComprehensiveBusiness
{
    public interface ITloSecuredParties : ITloDebtors
    {
        string Assignee { get; set; }
    }
}