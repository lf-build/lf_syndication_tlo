﻿using System.Collections.Generic;

namespace LendFoundry.Syndication.Tloxp.CreditReportResponse.ComprehensiveBusiness
{
    public interface IAddress
    {
        string Zip4 { get; set; }

        string CountryName { get; set; }

        IDate DateFirstSeen { get; set; }

        IDate DateLastSeen { get; set; }

        string BuildingName { get; set; }

        string Description { get; set; }

        string SubdivisionName { get; set; }

        string Distance { get; set; }

        string AddressMissingUnitDesignation { get; set; }

        string ReportToken { get; set; }

        string Link { get; set; }

        List<string> PropertyPhotoURLs { get; set; }
        string PropertyPhotosAddress { get; set; }
        string Line1 { get; set; }
        string Line2 { get; set; }

        string Line3 { get; set; }
        string City { get; set; }
        string State { get; set; }

        string Zip { get; set; }
        string County { get; set; }
        double Latitude { get; set; }
        double Longitude { get; set; }
    }
}