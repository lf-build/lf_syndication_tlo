﻿namespace LendFoundry.Syndication.Tloxp.CreditReportResponse.ComprehensiveBusiness
{
    public interface ITloBasicSSNRecord
    {
        string SSN { get; set; }

        string SSNPlaceOfIssue { get; set; }

        string SSNIssueYears { get; set; }

        string First5Masked { get; set; }

        string Last4Masked { get; set; }
    }
}