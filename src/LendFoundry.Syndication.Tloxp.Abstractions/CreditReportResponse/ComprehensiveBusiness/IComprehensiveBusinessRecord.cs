﻿using System.Collections.Generic;

namespace LendFoundry.Syndication.Tloxp.CreditReportResponse.ComprehensiveBusiness
{
    public interface IComprehensiveBusinessRecord
    {
        List<IBusinessName> BusinessNames { get; set; }

        string FeiNumber { get; set; }

        string Industry { get; set; }

        List<string> Industries { get; set; }
    }
}