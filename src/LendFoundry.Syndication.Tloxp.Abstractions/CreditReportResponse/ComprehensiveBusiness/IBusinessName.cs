﻿namespace LendFoundry.Syndication.Tloxp.CreditReportResponse.ComprehensiveBusiness
{
    public interface IBusinessName
    {
        string Name { get; set; }
        string Type { get; set; }
    }
}