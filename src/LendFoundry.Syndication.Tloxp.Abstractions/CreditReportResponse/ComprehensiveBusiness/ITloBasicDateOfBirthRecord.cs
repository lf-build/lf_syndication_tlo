﻿namespace LendFoundry.Syndication.Tloxp.CreditReportResponse.ComprehensiveBusiness
{
    public interface ITloBasicDateOfBirthRecord
    {
        IDate DateOfBirth { get; set; }

        string CurrentAge { get; set; }

        string Suppressed { get; set; }

        string YearMasked { get; set; }

        string MonthMasked { get; set; }

        string DayMasked { get; set; }
    }
}