﻿namespace LendFoundry.Syndication.Tloxp.CreditReportResponse.ComprehensiveBusiness
{
    public interface ITloBasicName
    {
        string Title { get; set; }

        string ProfessionalSuffix { get; set; }

        IDate DateFirstSeen { get; set; }

        IDate DateLastSeen { get; set; }
        string FirstName { get; set; }

        string MiddleName { get; set; }

        string LastName { get; set; }

        string NameSuffix { get; set; }
    }
}