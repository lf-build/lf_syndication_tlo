﻿namespace LendFoundry.Syndication.Tloxp.CreditReportResponse.ComprehensiveBusiness
{
    public interface ITloBusinessCorporateFiling
    {
        string fEINumber { get; set; }
        string sIC { get; set; }
    }
}
