﻿namespace LendFoundry.Syndication.Tloxp.CreditReportResponse.ComprehensiveBusiness
{
    public interface ITloBusinessBankruptcy
    {
        IDate FilingDate { get; set; }
        string FilingChapterNumber { get; set; }
        string Status { get; set; }
        IDate StatusDate { get; set; }
    }
}