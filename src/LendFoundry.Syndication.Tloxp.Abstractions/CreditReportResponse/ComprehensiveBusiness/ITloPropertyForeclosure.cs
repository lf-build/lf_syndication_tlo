﻿using System.Collections.Generic;

namespace LendFoundry.Syndication.Tloxp.CreditReportResponse.ComprehensiveBusiness
{
    public interface ITloPropertyForeclosure
    {
        string FIPSCounty { get; set; }
        string AmountOfDefault { get; set; }
        string FinalJudgmentAmount { get; set; }
        string CourtCaseNumber { get; set; }
        IDate FilingDate { get; set; }
        List<ITloBasicName> Owners { get; set; }

        IAddress Address { get; set; }

        IAddress MailingAddress { get; set; }

        IAddress AuctionCallAddress { get; set; }

        ITloBasicName Lender { get; set; }

        IAddress LenderAddress { get; set; }

        IAddress TrusteeAddress { get; set; }
    }
}