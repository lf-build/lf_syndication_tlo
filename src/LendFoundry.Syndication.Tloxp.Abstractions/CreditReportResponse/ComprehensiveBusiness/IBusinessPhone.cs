﻿namespace LendFoundry.Syndication.Tloxp.CreditReportResponse.ComprehensiveBusiness
{
    public interface IBusinessPhone
    {
        string Phone { get; set; }
    }
}