﻿namespace LendFoundry.Syndication.Tloxp.CreditReportResponse.ComprehensiveBusiness
{
    public interface IBusinessContact
    {
        IAddress Address { get; set; }

        IDate DateOfBirth { get; set; }
        string BusinessName { get; set; }
        string Title { get; set; }
        string ReportToken { get; set; }

        string ContactType { get; set; }

        IDate DateFirstSeen { get; set; }

        IDate DateLastSeen { get; set; }

        string CriminalRecordsFound { get; set; }

        string BankruptcyRecordsFound { get; set; }

        string UCCFilingsFound { get; set; }

        string LiensFound { get; set; }
        string JudgmentsFound { get; set; }

        string BusinessAssociationsCount { get; set; }

        string CorporateAssociationsCount { get; set; }

        string IndividualSearchedFor { get; set; }
    }
}