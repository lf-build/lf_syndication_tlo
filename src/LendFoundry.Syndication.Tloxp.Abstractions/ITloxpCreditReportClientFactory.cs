﻿using LendFoundry.Security.Tokens;

namespace LendFoundry.Syndication.Tloxp
{
    public interface ITloxpCreditReportClientFactory
    {
        ITloxpService Create(ITokenReader reader);
    }
}