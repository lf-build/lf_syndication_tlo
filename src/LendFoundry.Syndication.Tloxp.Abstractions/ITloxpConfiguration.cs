﻿using LendFoundry.Foundation.Client;
using System.Collections.Generic;
namespace LendFoundry.Syndication.Tloxp
{
    public interface ITloxpConfiguration : IDependencyConfiguration
    {
         string Username { get; set; }
         string Password { get; set; }
         string GlbPurpose { get; set; }
         string DppaPurpose { get; set; }
         string PermissibleUseCode { get; set; }
         string Version { get; set; }
         string Url { get; set; }
         IPersonOutputOptions PersonOutputOptions { get; set; }
         IBusinessOutputOptions BusinessOutputOptions { get; set; }
         int ExpirationInDays { get; set; }
         string ReportType { get; set; }
         string ConnectionString { get; set; }
         
    }
}