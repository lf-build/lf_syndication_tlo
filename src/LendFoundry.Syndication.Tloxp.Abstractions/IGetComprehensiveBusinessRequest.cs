﻿namespace LendFoundry.Syndication.Tloxp
{
  public interface IGetComprehensiveBusinessRequest
  {
    string BusinessToken { get; set; }

    string FEINumber { get; set; }
  }
}