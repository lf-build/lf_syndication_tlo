﻿namespace LendFoundry.Syndication.Tloxp
{
    public interface IBusinessOutputOptions
    {
        string ShowBankruptcies { get; set; }
        string ShowJudgements { get; set; }
        string ShowLiens { get; set; }
        string ShowPropertyForeclosures { get; set; }
        string ShowUCCFilings { get; set; }
        string ShowCorporateFilings { get; set; }
    }
}