namespace LendFoundry.Syndication.Tloxp
{
   public enum ReportType
    {
        PDF,
        TXT,
        RTF
    }
}
