﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.Tloxp
{
  public interface ITloxpService
  {

    Task<IComprehensivePersonReport> CreateComprehensivePersonReport(string entityType, string entityId, IGetComprehensivePersonRequest creditReportRequest);

    Task<IComprehensiveBusinessReport> CreateComprehensiveBusinessReport(string entityType, string entityId, IGetBusinessReportRequest creditReportRequest);

    Task<IComprehensiveBusinessReport> GetComprehensiveBusinessReport(string entityType, string entityId, string feinno, string reportId);

    Task<IEnumerable<IComprehensiveBusinessReport>> GetComprehensiveBusinessReports(string entityType, string entityId, string feinno, int? skip = default(int?), int? take = default(int?));

    Task<IComprehensivePersonReport> GetComprehensivePersonReport(string entityType, string entityId, string ssn, string reportId);

    Task<IEnumerable<IComprehensivePersonReport>> GetComprehensivePersonReports(string entityType, string entityId, string ssn, int? skip = default(int?), int? take = default(int?));

    Task<IComprehensiveBusinessReport> ComprehensiveBusinessReportBasedOnToken(string entityType, string entityId, IGetComprehensiveBusinessRequest creditReportRequest);

    Task<IComprehensiveBusinessSearch> ComprehensiveBusinessSearch(string entityType, string entityId, IGetBusinessReportRequest businessSearchRequest);
  }
}