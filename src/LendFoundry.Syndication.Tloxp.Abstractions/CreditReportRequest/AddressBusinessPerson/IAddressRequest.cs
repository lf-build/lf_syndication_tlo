﻿namespace LendFoundry.Syndication.Tloxp.CreditReportRequest.AddressBusinessPerson
{
  public interface IAddressRequest
  {
    string Line1 { get; set; }
    string Line2 { get; set; }
    string Line3 { get; set; }
    string City { get; set; }
    string State { get; set; }
    string Zip { get; set; }
    string County { get; set; }
  }
}