﻿using System;

namespace LendFoundry.Syndication.Tloxp
{
    public class Settings
    {
        public static string ServiceName => Environment.GetEnvironmentVariable($"CONFIGURATION_NAME") ?? "tloxp";
    }
}