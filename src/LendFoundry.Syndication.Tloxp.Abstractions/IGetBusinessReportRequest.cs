﻿using LendFoundry.Syndication.Tloxp.CreditReportRequest.AddressBusinessPerson;
namespace LendFoundry.Syndication.Tloxp
{
  public interface IGetBusinessReportRequest
  {
    string FEINumber { get; set; }

    string DunsNumber { get; set; }

    string BusinessName { get; set; }

    AddressRequest Address { get; set; }
  }
}