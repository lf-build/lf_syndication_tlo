
using System;

namespace LendFoundry.Syndication.Tloxp
{
  public interface IComprehensiveBusinessSearch
  {
    object BusinessSearchOutputRecords { get; set; }

  }
}