﻿using LendFoundry.Foundation.Persistence;
using System;

namespace LendFoundry.Syndication.Tloxp
{
    public class ComprehensivePersonReport : Aggregate, IComprehensivePersonReport
    {
        public string EntityType { get; set; }

        public string EntityId { get; set; }
        public string SSN { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }

        public object Report { get; set; }
        public DateTimeOffset ReportDate { get; set; }
        public DateTimeOffset ExpirationDate { get; set; }
    }
}