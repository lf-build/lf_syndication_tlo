﻿namespace LendFoundry.Syndication.Tloxp
{
    public interface IComprehensivePersonReportRequest
    {
        string EntityType { get; set; }
        string EntityId { get; set; }
        string SSN { get; set; }
        string FirstName { get; set; }
        string LastName { get; set; }
    }
}