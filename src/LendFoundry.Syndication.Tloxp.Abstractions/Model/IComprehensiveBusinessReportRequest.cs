﻿namespace LendFoundry.Syndication.Tloxp
{
    public interface IComprehensiveBusinessReportRequest
    {
        string EntityType { get; set; }
        string EntityId { get; set; }
        string FEINumber { get; set; }
        string DunsNumber { get; set; }
    }
}