﻿using LendFoundry.Foundation.Persistence;
using System;

namespace LendFoundry.Syndication.Tloxp
{
    public class ComprehensiveBusinessReport : Aggregate, IComprehensiveBusinessReport
    {
        public string EntityType { get; set; }

        public string EntityId { get; set; }

        public string FeinNumber { get; set; }
        public string DunsNumber { get; set; }

        public object Report { get; set; }
        public DateTimeOffset ReportDate { get; set; }
        public DateTimeOffset ExpirationDate { get; set; }
    }
}