﻿namespace LendFoundry.Syndication.Tloxp
{
    public class ComprehensivePersonReportRequest : IComprehensivePersonReportRequest
    {
        public string EntityType { get; set; }
        public string EntityId { get; set; }
        public string SSN { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
}