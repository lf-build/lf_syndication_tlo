﻿using LendFoundry.Foundation.Persistence;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.Tloxp
{
    public interface IComprehensivePersonReportRepository : IRepository<IComprehensivePersonReport>
    {
        Task<IComprehensivePersonReport> Get(string entityType, string entityId, string ssn, string firstName);

        Task<IComprehensivePersonReport> GetReport(string entityType, string entityId, string ssn, string reportId);
    }
}