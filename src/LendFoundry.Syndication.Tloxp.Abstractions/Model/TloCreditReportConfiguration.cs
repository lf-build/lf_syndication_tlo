﻿namespace LendFoundry.Syndication.Tloxp
{
    public sealed class TloCreditReportConfiguration
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public string GlbPurpose { get; set; }
        public string DppaPurpose { get; set; }
        public string PermissibleUseCode { get; set; }
        public string Version { get; set; }
        public string Url { get; set; }
        public int ExpirationInDays { get; set; }
    }
}