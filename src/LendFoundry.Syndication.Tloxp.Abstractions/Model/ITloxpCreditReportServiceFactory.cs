﻿using LendFoundry.Foundation.Logging;
using LendFoundry.Security.Tokens;

namespace LendFoundry.Syndication.Tloxp
{
    public interface ITloxpServiceFactory
    {
        ITloxpService Create(ITokenReader reader, ILogger logger);
    }
}