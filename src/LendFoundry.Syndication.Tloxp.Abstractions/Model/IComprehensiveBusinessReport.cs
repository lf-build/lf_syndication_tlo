﻿using LendFoundry.Foundation.Persistence;
using System;

namespace LendFoundry.Syndication.Tloxp
{
    public interface IComprehensiveBusinessReport : IAggregate
    {
        string EntityType { get; set; }

        string EntityId { get; set; }

        string FeinNumber { get; set; }
        string DunsNumber { get; set; }
        object Report { get; set; }
        DateTimeOffset ReportDate { get; set; }
        DateTimeOffset ExpirationDate { get; set; }
    }
}