﻿using LendFoundry.Foundation.Persistence;
using System;

namespace LendFoundry.Syndication.Tloxp
{
    public interface IComprehensivePersonReport : IAggregate
    {
        string EntityType { get; set; }
        string EntityId { get; set; }
        string SSN { get; set; }
        string FirstName { get; set; }
        string LastName { get; set; }
        object Report { get; set; }
        DateTimeOffset ReportDate { get; set; }
        DateTimeOffset ExpirationDate { get; set; }
    }
}