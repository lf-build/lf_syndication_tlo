﻿using LendFoundry.Foundation.Persistence;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.Tloxp
{
    public interface IComprehensiveBusinessReportRepository : IRepository<IComprehensiveBusinessReport>
    {
        Task<IComprehensiveBusinessReport> Get(string entityType, string entityId, string feinno);

        Task<IComprehensiveBusinessReport> GetReport(string entityType, string entityId, string feinno, string reportId);
    }
}