﻿namespace LendFoundry.Syndication.Tloxp
{
    public class ComprehensiveBusinessReportRequest : IComprehensiveBusinessReportRequest
    {
        public string EntityType { get; set; }
        public string EntityId { get; set; }
        public string FEINumber { get; set; }
        public string DunsNumber { get; set; }
    }
}