﻿namespace LendFoundry.Syndication.Tloxp
{
    public interface IPersonOutputOptions
    {
        string ShowPropertyForeclosures { get; set; }
        string ShowUccFilings { get; set; }
        string ShowJudgments { get; set; }
        string ShowLiens { get; set; }
        string ShowBankruptcies { get; set; }
        string ShowCriminalRecords { get; set; }
    }
}