﻿using LendFoundry.Syndication.Tloxp.CreditReportResponse.ComprehensiveBusiness;
using System.Collections.Generic;

namespace LendFoundry.Syndication.Tloxp
{
    public interface IGetComprehensiveBusinessResponse
    {
        IComprehensiveBusinessRecord BusinessRecord { get; set; }
        IDate IncorporationDate { get; set; }
        List<ITloLienFiling> LiensFiling { get; set; }
        List<ITloJudgmentFiling> JugdmentFiling { get; set; }
        List<ITloUCCFiling> UccFiling { get; set; }
        List<ITloBusinessBankruptcy> TloBusinessBankruptcy { get; set; }
        List<ITloPropertyForeclosure> ProperyForeclosure { get; set; }
        List<ITloBusinessCorporateFiling> CoorporateFiling { get; set; }
    }
}